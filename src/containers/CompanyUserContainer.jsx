import React from 'react'
import {connect} from 'react-redux'
import apiBaseUrl from './../AppConfig.jsx';
import cookie from 'react-cookies';
import CompanyUser from "../views/CompanyUser.jsx";


let Companies = (props) => {
    return (
        <CompanyUser {...props}/>
    )
};

const mapStateToProps = (state, ownProps) => {
    return state.CompanyUserReducer.toJS();
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        all: async (companyId) => {
            let companyUsers = await fetch(apiBaseUrl + '/admin/company-users/' + companyId, {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'get'
            }).then((response) => {
                return response.json();
            });
            if (companyUsers.success) {
                dispatch({
                    type: 'SET_COMPANY_USERS',
                    data: companyUsers.data.companyUsers
                });
                dispatch({
                    type: 'MUST_GET_COMPANY_USERS',
                    data: []
                });
            }
        },
        get: async (id) => {
            let companyUser = await fetch(apiBaseUrl + '/admin/company-user/' + id, {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'get'
            }).then((response) => {
                return response.json();
            });
            if (companyUser.success) {
                dispatch({
                    type: 'SET_COMPANY_USER',
                    data: companyUser.data.companyUser
                });
                dispatch({
                    type: 'GET_COMPANY_USER',
                    data: []
                });
            }
        },
        add: async (companyId, name, type, email, username, password, confirmPassword) => {
            let companyUser = await fetch(apiBaseUrl + '/admin/company-user/create', {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'post',
                body: JSON.stringify({
                    company_id: companyId,
                    name: name,
                    type: type,
                    email: email,
                    username: username,
                    password: password,
                    confirm_password: confirmPassword
                })
            }).then((response) => {
                return response.json();
            });
            if (companyUser.success) {
                dispatch({
                    type: 'SET_COMPANY_USER',
                    data: companyUser.data.companyUser
                });
                dispatch({
                    type: 'COMPANY_USER_CREATED',
                    data: []
                });
            } else {
                dispatch({
                    type: 'CREATE_COMPANY_USER_ERRORS',
                    data: companyUser.errors
                });
            }
        },
        edit: async (id, companyId, name, type, email, username, password, confirmPassword) => {
            let companyUser = await fetch(apiBaseUrl + '/admin/company-user/edit/' + id, {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'post',
                body: JSON.stringify({
                    company_id: companyId,
                    name: name,
                    type: type,
                    email: email,
                    username: username,
                    password: password,
                    confirm_password: confirmPassword
                })
            }).then((response) => {
                return response.json();
            });
            if (companyUser.success) {
                dispatch({
                    type: 'SET_COMPANY_USER',
                    data: companyUser.data.companyUser
                });
                dispatch({
                    type: 'COMPANY_USER_EDITED',
                    data: []
                });
            } else {
                dispatch({
                    type: 'EDIT_COMPANY_USER_ERRORS',
                    data: companyUser.errors
                });
            }
        },
        delete: async (id) => {
            let companyUser = await fetch(apiBaseUrl + '/admin/company-user/delete/' + id, {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'get'
            }).then((response) => {
                return response.json();
            });
        },
        setDefaults: async () => {
            dispatch({
                type: 'UNSET_COMPANY_USER_CREATED',
                data: []
            });
            dispatch({
                type: 'UNSET_COMPANY_USER_EDITED',
                data: []
            });
            dispatch({
                type: 'UNSET_MUST_GET_COMPANY_USERS',
                data: []
            });
            dispatch({
                type: 'UNSET_COMPANY_USER_GET',
                data: []
            });
            dispatch({
                type: 'UNSET_MUST_GET_COMPANY_USER',
                data: []
            });
            dispatch({
                type: 'REMOVE_CREATE_COMPANY_USER_ERRORS',
                data: []
            });
            dispatch({
                type: 'REMOVE_EDIT_COMPANY_USER_ERRORS',
                data: []
            });
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Companies);