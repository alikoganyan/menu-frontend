import React from 'react'
import {connect} from 'react-redux'
import apiBaseUrl from './../AppConfig.jsx';
import cookie from 'react-cookies';
import Restaurant from "../views/Restaurant.jsx";


let Restaurants = (props) => {
    return (
        <Restaurant {...props}/>
    )
};

const mapStateToProps = (state, ownProps) => {
    return state.RestaurantReducer.toJS();
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        all: async () => {
            let restaurants = await fetch(apiBaseUrl + '/admin/restaurants', {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'get'
            }).then((response) => {
                return response.json();
            });
            if (restaurants.success) {
                dispatch({
                    type: 'SET_RESTAURANTS',
                    data: restaurants.data.restaurants
                });
                dispatch({
                    type: 'MUST_GET_RESTAURANTS',
                    data: []
                });
            }
        },
        get: async (id) => {
            let restaurant = await fetch(apiBaseUrl + '/admin/restaurant/' + id, {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'get'
            }).then((response) => {
                return response.json();
            });
            if (restaurant.success) {
                dispatch({
                    type: 'SET_RESTAURANT',
                    data: restaurant.data.restaurant
                });
                dispatch({
                    type: 'GET_RESTAURANT',
                    data: []
                });
            }
        },
        getRestaurantTypes: async () => {
            let restaurantTypes = await fetch(apiBaseUrl + '/admin/restaurant_types', {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'get'
            }).then((response) => {
                return response.json();
            });
            if (restaurantTypes.success) {
                dispatch({
                    type: 'SET_RESTAURANT_TYPES',
                    data: restaurantTypes.data.restaurantTypes
                });
                dispatch({
                    type: 'MUST_GET_RESTAURANT_TYPES',
                    data: []
                });
            }
        },
        getManagers: async () => {
            let managers = await fetch(apiBaseUrl + '/admin/object-managers', {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'get'
            }).then((response) => {
                return response.json();
            });
            if (managers.success) {
                dispatch({
                    type: 'SET_MANAGERS',
                    data: managers.data.managers
                });
                dispatch({
                    type: 'MUST_GET_MANAGERS',
                    data: []
                });
            }
        },
        add: async (name, phoneNumber, addressName, addressLat, addressLng, type, manager) => {
            let restaurant = await fetch(apiBaseUrl + '/admin/restaurant/create', {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'post',
                body: JSON.stringify({
                    name: name,
                    phone_number: phoneNumber,
                    address_name: addressName,
                    address_lat: addressLat,
                    address_lng: addressLng,
                    type_id: type,
                    manager_id: manager
                })
            }).then((response) => {
                return response.json();
            });
            if (restaurant.success) {
                dispatch({
                    type: 'SET_RESTAURANT',
                    data: restaurant.data.restaurant
                });
                dispatch({
                    type: 'RESTAURANT_CREATED',
                    data: []
                });
            } else {
                dispatch({
                    type: 'CREATE_RESTAURANT_ERRORS',
                    data: restaurant.errors
                });
            }
        },
        edit: async (id, name, phoneNumber, addressName, addressLat, addressLng, type, manager) => {
            let restaurant = await fetch(apiBaseUrl + '/admin/restaurant/edit/' + id, {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'post',
                body: JSON.stringify({
                    name: name,
                    phone_number: phoneNumber,
                    address_name: addressName,
                    address_lat: addressLat,
                    address_lng: addressLng,
                    type_id: type,
                    manager_id: manager
                })
            }).then((response) => {
                return response.json();
            });
            if (restaurant.success) {
                dispatch({
                    type: 'SET_RESTAURANT',
                    data: restaurant.data.restaurant
                });
                dispatch({
                    type: 'RESTAURANT_EDITED',
                    data: []
                });
            } else {
                dispatch({
                    type: 'EDIT_RESTAURANT_ERRORS',
                    data: restaurant.errors
                });
            }
        },
        delete: async (id) => {
            let restaurant = await fetch(apiBaseUrl + '/admin/restaurant/delete/' + id, {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'get'
            }).then((response) => {
                return response.json();
            });
        },
        setDefaults: async () => {
            dispatch({
                type: 'UNSET_RESTAURANT_CREATED',
                data: []
            });
            dispatch({
                type: 'UNSET_RESTAURANT_EDITED',
                data: []
            });
            dispatch({
                type: 'UNSET_MUST_GET_RESTAURANTS',
                data: []
            });
            dispatch({
                type: 'UNSET_MUST_GET_MANAGERS',
                data: []
            });
            dispatch({
                type: 'UNSET_MUST_GET_RESTAURANT_TYPES',
                data: []
            });
            dispatch({
                type: 'UNSET_RESTAURANT_GET',
                data: []
            });
            dispatch({
                type: 'UNSET_MUST_GET_RESTAURANT',
                data: []
            });
            dispatch({
                type: 'REMOVE_CREATE_RESTAURANT_ERRORS',
                data: []
            });
            dispatch({
                type: 'REMOVE_EDIT_RESTAURANT_ERRORS',
                data: []
            });
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Restaurants);