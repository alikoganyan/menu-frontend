import React from 'react'
import {connect} from 'react-redux'
import apiBaseUrl from './../AppConfig.jsx';
import cookie from 'react-cookies';
import Order from "../views/Order.jsx";


let Orders = (props) => {
    return (
        <Order {...props}/>
    )
};

const mapStateToProps = (state, ownProps) => {
    return state.OrderReducer.toJS();
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        all: async (companyId) => {
            let orders = await fetch(apiBaseUrl + '/admin/orders/' + companyId, {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'get'
            }).then((response) => {
                return response.json();
            });
            if (orders.success) {
                dispatch({
                    type: 'SET_ORDERS',
                    data: orders.data.orders
                });
                dispatch({
                    type: 'MUST_GET_ORDERS',
                    data: []
                });
            }
        },
        get: async (id) => {
            let order = await fetch(apiBaseUrl + '/admin/order/' + id, {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'get'
            }).then((response) => {
                return response.json();
            });
            if (order.success) {
                dispatch({
                    type: 'SET_ORDER',
                    data: order.data.order
                });
                dispatch({
                    type: 'GET_ORDER',
                    data: []
                });
            }
        },
        connect: async (orderId, driver) => {
            let order = await fetch(apiBaseUrl + '/admin/order/connect/' + orderId, {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'post',
                body: JSON.stringify({
                    driver: driver
                })
            }).then((response) => {
                return response.json();
            });
            if (order.success) {
                dispatch({
                    type: 'SET_ORDER',
                    data: order.data.order
                });
                dispatch({
                    type: 'ORDER_EDITED',
                    data: []
                });
            } else {
                dispatch({
                    type: 'EDIT_ORDER_ERRORS',
                    data: order.errors
                });
            }
        },
        cancel: async (orderId) => {
            let order = await fetch(apiBaseUrl + '/admin/order/cancel/' + orderId, {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'get',
            }).then((response) => {
                return response.json();
            });
            if (order.success) {
                dispatch({
                    type: 'ORDER_CANCEL',
                    data: order.data.order
                });
                dispatch({
                    type: 'ORDER_CANCELLED',
                    data: []
                });
            }
        },
        setDefaults: async () => {
            dispatch({
                type: 'UNSET_ORDER_CREATED',
                data: []
            });
            dispatch({
                type: 'UNSET_ORDER_CANCELLED',
                data: []
            });
            dispatch({
                type: 'UNSET_ORDER_EDITED',
                data: []
            });
            dispatch({
                type: 'UNSET_MUST_GET_ORDERS',
                data: []
            });
            dispatch({
                type: 'UNSET_ORDER_GET',
                data: []
            });
            dispatch({
                type: 'UNSET_ORDER_CANCEL',
                data: []
            });
            dispatch({
                type: 'UNSET_DRIVERS_GET',
                data: []
            });
            dispatch({
                type: 'UNSET_MUST_GET_ORDER',
                data: []
            });
            dispatch({
                type: 'REMOVE_CREATE_ORDER_ERRORS',
                data: []
            });
            dispatch({
                type: 'REMOVE_EDIT_ORDER_ERRORS',
                data: []
            });
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Orders);