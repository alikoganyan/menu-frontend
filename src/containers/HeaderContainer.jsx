import React from 'react'
import {connect} from 'react-redux'
import apiBaseUrl from './../AppConfig.jsx';
import cookie from 'react-cookies';
import Header from "../components/Header.jsx";


let Headers = (props) => {
    return (
        <Header {...props}/>
    )
};

const mapStateToProps = (state, ownProps) => {
    return state.UserReducer.toJS();
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        getUser: async () => {
            let user = await fetch(apiBaseUrl + '/me', {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'get'

            }).then((response) => {
                return response.json();
            });
            if (user.success) {
                dispatch({
                    type: 'GET_USER',
                    data: user.data.user
                });
            }
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Headers);