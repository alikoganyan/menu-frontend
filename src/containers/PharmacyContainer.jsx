import React from 'react'
import {connect} from 'react-redux'
import apiBaseUrl from './../AppConfig.jsx';
import cookie from 'react-cookies';
import Pharmacy from "../views/Pharmacy.jsx";


let Pharmacies = (props) => {
    return (
        <Pharmacy {...props}/>
    )
};

const mapStateToProps = (state, ownProps) => {
    return state.PharmacyReducer.toJS();
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        all: async () => {
            let pharmacies = await fetch(apiBaseUrl + '/admin/pharmacies', {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'get'
            }).then((response) => {
                return response.json();
            });
            if (pharmacies.success) {
                dispatch({
                    type: 'SET_PHARMACIES',
                    data: pharmacies.data.pharmacies
                });
                dispatch({
                    type: 'MUST_GET_PHARMACIES',
                    data: []
                });
            }
        },
        getManagers: async () => {
            let managers = await fetch(apiBaseUrl + '/admin/object-managers', {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'get'
            }).then((response) => {
                return response.json();
            });
            if (managers.success) {
                dispatch({
                    type: 'SET_MANAGERS',
                    data: managers.data.managers
                });
                dispatch({
                    type: 'MUST_GET_MANAGERS',
                    data: []
                });
            }
        },
        get: async (id) => {
            let pharmacy = await fetch(apiBaseUrl + '/admin/pharmacy/' + id, {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'get'
            }).then((response) => {
                return response.json();
            });
            if (pharmacy.success) {
                dispatch({
                    type: 'SET_PHARMACY',
                    data: pharmacy.data.pharmacy
                });
                dispatch({
                    type: 'GET_PHARMACY',
                    data: []
                });
            }
        },
        add: async (name, phoneNumber, addressName, addressLat, addressLng) => {
            let pharmacy = await fetch(apiBaseUrl + '/admin/pharmacy/create', {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'post',
                body: JSON.stringify({
                    name: name,
                    phone_number: phoneNumber,
                    address_name: addressName,
                    address_lat: addressLat,
                    address_lng: addressLng
                })
            }).then((response) => {
                return response.json();
            });
            if (pharmacy.success) {
                dispatch({
                    type: 'SET_PHARMACY',
                    data: pharmacy.data.pharmacy
                });
                dispatch({
                    type: 'PHARMACY_CREATED',
                    data: []
                });
            } else {
                dispatch({
                    type: 'CREATE_PHARMACY_ERRORS',
                    data: pharmacy.errors
                });
            }
        },
        edit: async (id, name, phoneNumber, addressName, addressLat, addressLng) => {
            let pharmacy = await fetch(apiBaseUrl + '/admin/pharmacy/edit/' + id, {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'post',
                body: JSON.stringify({
                    name: name,
                    phone_number: phoneNumber,
                    address_name: addressName,
                    address_lat: addressLat,
                    address_lng: addressLng,
                })
            }).then((response) => {
                return response.json();
            });
            if (pharmacy.success) {
                dispatch({
                    type: 'SET_PHARMACY',
                    data: pharmacy.data.pharmacy
                });
                dispatch({
                    type: 'PHARMACY_EDITED',
                    data: []
                });
            } else {
                dispatch({
                    type: 'EDIT_PHARMACY_ERRORS',
                    data: pharmacy.errors
                });
            }
        },
        delete: async (id) => {
            let pharmacy = await fetch(apiBaseUrl + '/admin/pharmacy/delete/' + id, {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'get'
            }).then((response) => {
                return response.json();
            });
        },
        setDefaults: async () => {
            dispatch({
                type: 'UNSET_PHARMACY_CREATED',
                data: []
            });
            dispatch({
                type: 'UNSET_PHARMACY_EDITED',
                data: []
            });
            dispatch({
                type: 'UNSET_MUST_GET_PHARMACIES',
                data: []
            });
            dispatch({
                type: 'UNSET_MUST_GET_MANAGERS',
                data: []
            });
            dispatch({
                type: 'UNSET_PHARMACY_GET',
                data: []
            });
            dispatch({
                type: 'UNSET_MUST_GET_PHARMACY',
                data: []
            });
            dispatch({
                type: 'REMOVE_CREATE_PHARMACY_ERRORS',
                data: []
            });
            dispatch({
                type: 'REMOVE_EDIT_PHARMACY_ERRORS',
                data: []
            });
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Pharmacies);