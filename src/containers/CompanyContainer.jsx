import React from 'react'
import {connect} from 'react-redux'
import apiBaseUrl from './../AppConfig.jsx';
import cookie from 'react-cookies';
import Company from "../views/Company.jsx";


let Companies = (props) => {
    return (
        <Company {...props}/>
    )
};

const mapStateToProps = (state, ownProps) => {
    return state.CompanyReducer.toJS();
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        all: async () => {
            let companies = await fetch(apiBaseUrl + '/admin/companies', {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'get'
            }).then((response) => {
                return response.json();
            });
            if (companies.success) {
                dispatch({
                    type: 'SET_COMPANIES',
                    data: companies.data.companies
                });
                dispatch({
                    type: 'MUST_GET_COMPANIES',
                    data: []
                });
            }
        },
        get: async (id) => {
            let company = await fetch(apiBaseUrl + '/admin/company/' + id, {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'get'
            }).then((response) => {
                return response.json();
            });
            if (company.success) {
                dispatch({
                    type: 'SET_COMPANY',
                    data: company.data.company
                });
                dispatch({
                    type: 'GET_COMPANY',
                    data: []
                });
            }
        },
        add: async (name, domain, email, username, password, confirmPassword) => {
            let company = await fetch(apiBaseUrl + '/admin/company/create', {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'post',
                body: JSON.stringify({
                    name: name,
                    domain: domain,
                    email: email,
                    username: username,
                    password: password,
                    confirm_password: confirmPassword
                })
            }).then((response) => {
                return response.json();
            });
            if (company.success) {
                dispatch({
                    type: 'SET_COMPANY',
                    data: company.data.company
                });
                dispatch({
                    type: 'COMPANY_CREATED',
                    data: []
                });
            } else {
                dispatch({
                    type: 'CREATE_COMPANY_ERRORS',
                    data: company.errors
                });
            }
        },
        edit: async (id, name, domain, email, username, password, confirmPassword) => {
            let company = await fetch(apiBaseUrl + '/admin/company/edit/' + id, {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'post',
                body: JSON.stringify({
                    name: name,
                    domain: domain,
                    email: email,
                    username: username,
                    password: password,
                    confirm_password: confirmPassword
                })
            }).then((response) => {
                return response.json();
            });
            if (company.success) {
                dispatch({
                    type: 'SET_COMPANY',
                    data: company.data.company
                });
                dispatch({
                    type: 'COMPANY_EDITED',
                    data: []
                });
            } else {
                dispatch({
                    type: 'EDIT_COMPANY_ERRORS',
                    data: company.errors
                });
            }
        },
        delete: async (id) => {
            let company = await fetch(apiBaseUrl + '/admin/company/delete/' + id, {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'get'
            }).then((response) => {
                return response.json();
            });
        },
        setDefaults: async () => {
            dispatch({
                type: 'UNSET_COMPANY_GET',
                data: []
            });
            dispatch({
                type: 'REMOVE_CREATE_COMPANY_ERRORS',
                data: []
            });
            dispatch({
                type: 'REMOVE_EDIT_COMPANY_ERRORS',
                data: []
            });
            dispatch({
                type: 'UNSET_COMPANY_CREATED',
                data: []
            });
            dispatch({
                type: 'UNSET_COMPANY_EDITED',
                data: []
            });
            dispatch({
                type: 'UNSET_MUST_GET_COMPANIES',
                data: []
            });
        },
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Companies);