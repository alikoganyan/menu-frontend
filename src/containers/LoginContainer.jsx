import React from 'react'
import {connect} from 'react-redux'
import Login from '../views/Login.jsx';
import apiBaseUrl from './../AppConfig.jsx';
import cookie from 'react-cookies';


let Logins = (props) => {
    return (
        <Login {...props}/>
    )
};

const mapStateToProps = (state, ownProps) => {
    return state.LoginReducer.toJS();
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {

        login: async (username, password) => {
            let login = await fetch(apiBaseUrl + '/admin/login', {
                headers: {
                    "Content-Type": "application/json"
                },
                method: 'post',
                body: JSON.stringify({
                    username: username,
                    password: password,
                }),

            }).then((response) => {
                return response.json();
            });
            if (login.success) {
                cookie.save('token', login.data.token);
                cookie.save('user', login.data.user);
                dispatch({
                    type: 'LOGIN_SUCCESS',
                    data: []
                });
            } else {
                dispatch({
                    type: 'LOGIN_ADD_ERRORS',
                    data: login.errors
                });
            }

        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Logins);