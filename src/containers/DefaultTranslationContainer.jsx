import React from 'react'
import {connect} from 'react-redux'
import apiBaseUrl from './../AppConfig.jsx';
import cookie from 'react-cookies';
import DefaultTranslation from "../views/DefaultTranslation.jsx";


let DefaultTranslations = (props) => {
    return (
        <DefaultTranslation {...props}/>
    )
};

const mapStateToProps = (state, ownProps) => {
    return state.DefaultTranslationReducer.toJS();
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        all: async (languageCode) => {
            let defaultTranslations = await fetch(apiBaseUrl + '/admin/default-translations/'+languageCode, {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'get'
            }).then((response) => {
                return response.json();
            });
            if (defaultTranslations.success) {
                dispatch({
                    type: 'SET_DEFAULT_TRANSLATIONS',
                    data: defaultTranslations.data.defaultTranslations
                });
                dispatch({
                    type: 'MUST_GET_DEFAULT_TRANSLATIONS',
                    data: []
                });
            }
        },
        edit: async (languageCode,translations) => {
            let defaultTranslations = await fetch(apiBaseUrl + '/admin/default-translations/' + languageCode, {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'post',
                body: JSON.stringify(translations)
            }).then((response) => {
                return response.json();
            });
            if (defaultTranslations.success) {
                dispatch({
                    type: 'SET_DEFAULT_TRANSLATION',
                    data: defaultTranslations.data.defaultTranslations
                });
                dispatch({
                    type: 'DEFAULT_TRANSLATION_EDITED',
                    data: []
                });
            } else {
                dispatch({
                    type: 'EDIT_DEFAULT_TRANSLATION_ERRORS',
                    data: defaultTranslations.errors
                });
            }
        },
        setDefaults: async () => {
            dispatch({
                type: 'UNSET_DEFAULT_TRANSLATION_EDITED',
                data: []
            });
            dispatch({
                type: 'UNSET_MUST_GET_DEFAULT_TRANSLATIONS',
                data: []
            });
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(DefaultTranslations);