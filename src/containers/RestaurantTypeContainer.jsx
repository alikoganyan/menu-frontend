import React from 'react'
import {connect} from 'react-redux'
import apiBaseUrl from './../AppConfig.jsx';
import cookie from 'react-cookies';
import RestaurantType from "../views/RestaurantType.jsx";


let RestaurantTypes = (props) => {
    return (
        <RestaurantType {...props}/>
    )
};

const mapStateToProps = (state, ownProps) => {
    return state.RestaurantTypeReducer.toJS();
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        all: async () => {
            let restaurantTypes = await fetch(apiBaseUrl + '/admin/restaurant_types', {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'get'
            }).then((response) => {
                return response.json();
            });
            if (restaurantTypes.success) {
                dispatch({
                    type: 'SET_RESTAURANT_TYPES',
                    data: restaurantTypes.data.restaurantTypes
                });
                dispatch({
                    type: 'MUST_GET_RESTAURANT_TYPES',
                    data: []
                });
            }
        },
        getLanguages: async (companyId) => {
            let languages = await fetch(apiBaseUrl + '/admin/languages/' + companyId, {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'get'
            }).then((response) => {
                return response.json();
            });
            if (languages.success) {
                dispatch({
                    type: 'SET_LANGUAGES',
                    data: languages.data.languages
                });
            }
        },
        get: async (id) => {
            let restaurantType = await fetch(apiBaseUrl + '/admin/restaurant_type/' + id, {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'get'
            }).then((response) => {
                return response.json();
            });
            if (restaurantType.success) {
                dispatch({
                    type: 'SET_RESTAURANT_TYPE',
                    data: restaurantType.data.restaurantType
                });
                dispatch({
                    type: 'GET_RESTAURANT_TYPE',
                    data: []
                });
            }
        },
        add: async (names) => {
            let restaurantType = await fetch(apiBaseUrl + '/admin/restaurant_type/create', {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'post',
                body: JSON.stringify(names)
            }).then((response) => {
                return response.json();
            });
            if (restaurantType.success) {
                dispatch({
                    type: 'SET_RESTAURANT_TYPE',
                    data: restaurantType.data.restaurantType
                });
                dispatch({
                    type: 'RESTAURANT_TYPE_CREATED',
                    data: []
                });
            } else {
                dispatch({
                    type: 'CREATE_RESTAURANT_TYPE_ERRORS',
                    data: restaurantType.errors
                });
            }
        },
        edit: async (id, names) => {
            let restaurantType = await fetch(apiBaseUrl + '/admin/restaurant_type/edit/' + id, {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'post',
                body: JSON.stringify(names)
            }).then((response) => {
                return response.json();
            });
            if (restaurantType.success) {
                dispatch({
                    type: 'SET_RESTAURANT_TYPE',
                    data: restaurantType.data.restaurantType
                });
                dispatch({
                    type: 'RESTAURANT_TYPE_EDITED',
                    data: []
                });
            } else {
                dispatch({
                    type: 'EDIT_RESTAURANT_TYPE_ERRORS',
                    data: restaurantType.errors
                });
            }
        },
        delete: async (id) => {
            let restaurantType = await fetch(apiBaseUrl + '/admin/restaurant_type/delete/' + id, {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'get'
            }).then((response) => {
                return response.json();
            });
        },
        setDefaults: async () => {
            dispatch({
                type: 'UNSET_RESTAURANT_TYPE_GET',
                data: []
            });
            dispatch({
                type: 'REMOVE_CREATE_RESTAURANT_TYPE_ERRORS',
                data: []
            });
            dispatch({
                type: 'REMOVE_EDIT_RESTAURANT_TYPE_ERRORS',
                data: []
            });
            dispatch({
                type: 'UNSET_RESTAURANT_TYPE_CREATED',
                data: []
            });
            dispatch({
                type: 'UNSET_RESTAURANT_TYPE_EDITED',
                data: []
            });
            dispatch({
                type: 'UNSET_MUST_GET_RESTAURANT_TYPES',
                data: []
            });
        },
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(RestaurantTypes);