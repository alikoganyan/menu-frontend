import React from 'react'
import {connect} from 'react-redux'
import apiBaseUrl from './../AppConfig.jsx';
import cookie from 'react-cookies';
import Language from "../views/Language.jsx";


let Languages = (props) => {
    return (
        <Language {...props}/>
    )
};

const mapStateToProps = (state, ownProps) => {
    return state.LanguageReducer.toJS();
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        all: async (companyId) => {
            let languages = await fetch(apiBaseUrl + '/admin/languages/'+companyId, {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'get'
            }).then((response) => {
                return response.json();
            });
            if (languages.success) {
                dispatch({
                    type: 'SET_LANGUAGES',
                    data: languages.data.languages
                });
                dispatch({
                    type: 'MUST_GET_LANGUAGES',
                    data: []
                });
            }
        },
        get: async (id) => {
            let language = await fetch(apiBaseUrl + '/admin/language/' + id, {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'get'
            }).then((response) => {
                return response.json();
            });
            if (language.success) {
                dispatch({
                    type: 'SET_LANGUAGE',
                    data: language.data.language
                });
                dispatch({
                    type: 'GET_LANGUAGE',
                    data: []
                });
            }
        },
        add: async (name, phoneNumber, addressName, addressLat, addressLng) => {
            let language = await fetch(apiBaseUrl + '/admin/language/create', {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'post',
                body: JSON.stringify({
                    name: name,
                    phone_number: phoneNumber,
                    address_name: addressName,
                    address_lat: addressLat,
                    address_lng: addressLng
                })
            }).then((response) => {
                return response.json();
            });
            if (language.success) {
                dispatch({
                    type: 'SET_LANGUAGE',
                    data: language.data.language
                });
                dispatch({
                    type: 'LANGUAGE_CREATED',
                    data: []
                });
            } else {
                dispatch({
                    type: 'CREATE_LANGUAGE_ERRORS',
                    data: language.errors
                });
            }
        },
        edit: async (id, name, phoneNumber, addressName, addressLat, addressLng) => {
            let language = await fetch(apiBaseUrl + '/admin/language/edit/' + id, {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'post',
                body: JSON.stringify({
                    name: name,
                    phone_number: phoneNumber,
                    address_name: addressName,
                    address_lat: addressLat,
                    address_lng: addressLng,
                })
            }).then((response) => {
                return response.json();
            });
            if (language.success) {
                dispatch({
                    type: 'SET_LANGUAGE',
                    data: language.data.language
                });
                dispatch({
                    type: 'LANGUAGE_EDITED',
                    data: []
                });
            } else {
                dispatch({
                    type: 'EDIT_LANGUAGE_ERRORS',
                    data: language.errors
                });
            }
        },
        delete: async (id) => {
            let language = await fetch(apiBaseUrl + '/admin/language/delete/' + id, {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'get'
            }).then((response) => {
                return response.json();
            });
        },
        setDefaults: async () => {
            dispatch({
                type: 'UNSET_LANGUAGE_CREATED',
                data: []
            });
            dispatch({
                type: 'UNSET_LANGUAGE_EDITED',
                data: []
            });
            dispatch({
                type: 'UNSET_MUST_GET_LANGUAGES',
                data: []
            });
            dispatch({
                type: 'UNSET_LANGUAGE_GET',
                data: []
            });
            dispatch({
                type: 'UNSET_MUST_GET_LANGUAGE',
                data: []
            });
            dispatch({
                type: 'REMOVE_CREATE_LANGUAGE_ERRORS',
                data: []
            });
            dispatch({
                type: 'REMOVE_EDIT_LANGUAGE_ERRORS',
                data: []
            });
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Languages);