import React, {Component} from 'react';
import {Link, Switch, Route, Redirect} from 'react-router-dom';
import {Container} from 'reactstrap';
import Header from './HeaderContainer.jsx';
import Sidebar from '../components/Sidebar/Sidebar.jsx';
import Breadcrumb from '../components/Breadcrumb/';
import Aside from '../components/Aside/';
import Footer from '../components/Footer/Footer';

import Dashboard from '../views/Dashboard/';
import PrivateRoute from "../PrivateRoute.jsx";
import Company from "./CompanyContainer.jsx";
import CompanyUserContainer from "./CompanyUserContainer.jsx";
import ShopContainer from "./ShopContainer.jsx";
import RestaurantContainer from "./RestaurantContainer.jsx";
import PharmacyContainer from "./PharmacyContainer.jsx";
import LanguageContainer from "./LanguageContainer.jsx";
import DefaultTranslationContainer from "./DefaultTranslationContainer.jsx";
import CategoryContainer from "./CategoryContainer.jsx";
import RestaurantTypeContainer from "./RestaurantTypeContainer.jsx";
import ProductSectionContainer from "./ProductSectionContainer.jsx";
import ProductContainer from "./ProductContainer.jsx";
import OrderContainer from "./OrderContainer.jsx";
import AdminRoute from "../AdminRoute.jsx";


class Full extends Component {
    constructor(props) {
        super(props);

    }

    render() {
        return (
            <div className="app">
                <link rel={'stylesheet'}
                      href={'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css'}></link>
                <link rel={'stylesheet'}
                      href={'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css'}></link>
                <Header/>
                <div className="app-body">
                    <Sidebar userData={this.props}/>
                    <main className="main">
                        <Breadcrumb/>
                        <Container fluid>
                            <Switch>
                                <PrivateRoute path="/dashboard" name="Dashboard" component={Dashboard}/>
                                <AdminRoute path="/companies" name="Companies" component={Company}/>
                                <PrivateRoute path="/company-users/:companyId?" name="Company Users"
                                              component={CompanyUserContainer}/>
                                <PrivateRoute path="/shops" name="Shops"
                                              component={ShopContainer}/>
                                <PrivateRoute path="/restaurants" name="Restaurants"
                                              component={RestaurantContainer}/>
                                <PrivateRoute path="/pharmacies" name="Pharmacies"
                                              component={PharmacyContainer}/>
                                <PrivateRoute path="/languages" name="Languages"
                                              component={LanguageContainer}/>
                                <PrivateRoute path="/default-translations/:languageCode" name="Default translations"
                                              component={DefaultTranslationContainer}/>
                                <PrivateRoute path="/categories" name="Categories"
                                              component={CategoryContainer}/>
                                <PrivateRoute path="/restaurant-types" name="Restaurant Types"
                                              component={RestaurantTypeContainer}/>
                                <PrivateRoute path="/product-sections/:objectId" name="Product Section"
                                              component={ProductSectionContainer}/>
                                <PrivateRoute path="/products/:sectionId" name="Products"
                                              component={ProductContainer}/>
                                <PrivateRoute path="/orders/:companyId?" name="Orders"
                                              component={OrderContainer}/>
                                <Redirect from="/" to="/dashboard"/>
                            </Switch>
                        </Container>
                    </main>
                    <Aside/>
                </div>
                <Footer/>
            </div>
        );
    }
}

export default Full;
