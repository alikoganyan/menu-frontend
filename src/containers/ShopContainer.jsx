import React from 'react'
import {connect} from 'react-redux'
import apiBaseUrl from './../AppConfig.jsx';
import cookie from 'react-cookies';
import Shop from "../views/Shop.jsx";


let Shops = (props) => {
    return (
        <Shop {...props}/>
    )
};

const mapStateToProps = (state, ownProps) => {
    return state.ShopReducer.toJS();
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        all: async () => {
            let shops = await fetch(apiBaseUrl + '/admin/shops', {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'get'
            }).then((response) => {
                return response.json();
            });
            if (shops.success) {
                dispatch({
                    type: 'SET_SHOPS',
                    data: shops.data.shops
                });
                dispatch({
                    type: 'MUST_GET_SHOPS',
                    data: []
                });
            }
        },
        getManagers: async () => {
            let managers = await fetch(apiBaseUrl + '/admin/object-managers', {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'get'
            }).then((response) => {
                return response.json();
            });
            if (managers.success) {
                dispatch({
                    type: 'SET_MANAGERS',
                    data: managers.data.managers
                });
                dispatch({
                    type: 'MUST_GET_MANAGERS',
                    data: []
                });
            }
        },
        get: async (id) => {
            let shop = await fetch(apiBaseUrl + '/admin/shop/' + id, {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'get'
            }).then((response) => {
                return response.json();
            });
            if (shop.success) {
                dispatch({
                    type: 'SET_SHOP',
                    data: shop.data.shop
                });
                dispatch({
                    type: 'GET_SHOP',
                    data: []
                });
            }
        },
        getCategories: async () => {
            let categories = await fetch(apiBaseUrl + '/admin/categories', {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'get'
            }).then((response) => {
                return response.json();
            });
            if (categories.success) {
                dispatch({
                    type: 'SET_CATEGORIES',
                    data: categories.data.categories
                });
                dispatch({
                    type: 'MUST_GET_CATEGORIES',
                    data: []
                });
            }
        },
        add: async (name, phoneNumber, addressName, addressLat, addressLng, category, manager) => {
            let shop = await fetch(apiBaseUrl + '/admin/shop/create', {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'post',
                body: JSON.stringify({
                    name: name,
                    phone_number: phoneNumber,
                    address_name: addressName,
                    address_lat: addressLat,
                    address_lng: addressLng,
                    category_id: category,
                    manager_id: manager
                })
            }).then((response) => {
                return response.json();
            });
            if (shop.success) {
                dispatch({
                    type: 'SET_SHOP',
                    data: shop.data.shop
                });
                dispatch({
                    type: 'SHOP_CREATED',
                    data: []
                });
            } else {
                dispatch({
                    type: 'CREATE_SHOP_ERRORS',
                    data: shop.errors
                });
            }
        },
        edit: async (id, name, phoneNumber, addressName, addressLat, addressLng, category, manager) => {
            let shop = await fetch(apiBaseUrl + '/admin/shop/edit/' + id, {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'post',
                body: JSON.stringify({
                    name: name,
                    phone_number: phoneNumber,
                    address_name: addressName,
                    address_lat: addressLat,
                    address_lng: addressLng,
                    category_id: category,
                    manager_id: manager
                })
            }).then((response) => {
                return response.json();
            });
            if (shop.success) {
                dispatch({
                    type: 'SET_SHOP',
                    data: shop.data.shop
                });
                dispatch({
                    type: 'SHOP_EDITED',
                    data: []
                });
            } else {
                dispatch({
                    type: 'EDIT_SHOP_ERRORS',
                    data: shop.errors
                });
            }
        },
        delete: async (id) => {
            let shop = await fetch(apiBaseUrl + '/admin/shop/delete/' + id, {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'get'
            }).then((response) => {
                return response.json();
            });
        },
        setDefaults: async () => {
            dispatch({
                type: 'UNSET_SHOP_CREATED',
                data: []
            });
            dispatch({
                type: 'UNSET_SHOP_EDITED',
                data: []
            });
            dispatch({
                type: 'UNSET_MUST_GET_SHOPS',
                data: []
            });
            dispatch({
                type: 'UNSET_MUST_GET_CATEGORIES',
                data: []
            });
            dispatch({
                type: 'UNSET_SHOP_GET',
                data: []
            });
            dispatch({
                type: 'UNSET_MUST_GET_SHOP',
                data: []
            });
            dispatch({
                type: 'REMOVE_CREATE_SHOP_ERRORS',
                data: []
            });
            dispatch({
                type: 'REMOVE_EDIT_SHOP_ERRORS',
                data: []
            });
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Shops);