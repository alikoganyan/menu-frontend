import React from 'react'
import {connect} from 'react-redux'
import apiBaseUrl from './../AppConfig.jsx';
import cookie from 'react-cookies';
import Category from "../views/Category.jsx";


let Categories = (props) => {
    return (
        <Category {...props}/>
    )
};

const mapStateToProps = (state, ownProps) => {
    return state.CategoryReducer.toJS();
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        all: async () => {
            let categories = await fetch(apiBaseUrl + '/admin/categories', {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'get'
            }).then((response) => {
                return response.json();
            });
            if (categories.success) {
                dispatch({
                    type: 'SET_CATEGORIES',
                    data: categories.data.categories
                });
                dispatch({
                    type: 'MUST_GET_CATEGORIES',
                    data: []
                });
            }
        },
        getLanguages: async (companyId) => {
            let languages = await fetch(apiBaseUrl + '/admin/languages/' + companyId, {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'get'
            }).then((response) => {
                return response.json();
            });
            if (languages.success) {
                dispatch({
                    type: 'SET_LANGUAGES',
                    data: languages.data.languages
                });
            }
        },
        get: async (id) => {
            let category = await fetch(apiBaseUrl + '/admin/category/' + id, {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'get'
            }).then((response) => {
                return response.json();
            });
            if (category.success) {
                dispatch({
                    type: 'SET_CATEGORY',
                    data: category.data.category
                });
                dispatch({
                    type: 'GET_CATEGORY',
                    data: []
                });
            }
        },
        add: async (names) => {
            let category = await fetch(apiBaseUrl + '/admin/category/create', {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'post',
                body: JSON.stringify(names)
            }).then((response) => {
                return response.json();
            });
            if (category.success) {
                dispatch({
                    type: 'SET_CATEGORY',
                    data: category.data.category
                });
                dispatch({
                    type: 'CATEGORY_CREATED',
                    data: []
                });
            } else {
                dispatch({
                    type: 'CREATE_CATEGORY_ERRORS',
                    data: category.errors
                });
            }
        },
        edit: async (id, names) => {
            let category = await fetch(apiBaseUrl + '/admin/category/edit/' + id, {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'post',
                body: JSON.stringify(names)
            }).then((response) => {
                return response.json();
            });
            if (category.success) {
                dispatch({
                    type: 'SET_CATEGORY',
                    data: category.data.category
                });
                dispatch({
                    type: 'CATEGORY_EDITED',
                    data: []
                });
            } else {
                dispatch({
                    type: 'EDIT_CATEGORY_ERRORS',
                    data: category.errors
                });
            }
        },
        delete: async (id) => {
            let category = await fetch(apiBaseUrl + '/admin/category/delete/' + id, {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'get'
            }).then((response) => {
                return response.json();
            });
        },
        setDefaults: async () => {
            dispatch({
                type: 'UNSET_CATEGORY_GET',
                data: []
            });
            dispatch({
                type: 'REMOVE_CREATE_CATEGORY_ERRORS',
                data: []
            });
            dispatch({
                type: 'REMOVE_EDIT_CATEGORY_ERRORS',
                data: []
            });
            dispatch({
                type: 'UNSET_CATEGORY_CREATED',
                data: []
            });
            dispatch({
                type: 'UNSET_CATEGORY_EDITED',
                data: []
            });
            dispatch({
                type: 'UNSET_MUST_GET_CATEGORIES',
                data: []
            });
        },
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Categories);