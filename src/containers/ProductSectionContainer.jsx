import React from 'react'
import {connect} from 'react-redux'
import apiBaseUrl from './../AppConfig.jsx';
import cookie from 'react-cookies';
import ProductSection from "../views/ProductSection.jsx";


let ProductSections = (props) => {
    return (
        <ProductSection {...props}/>
    )
};

const mapStateToProps = (state, ownProps) => {
    return state.ProductSectionReducer.toJS();
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        all: async (objectId) => {
            let productSections = await fetch(apiBaseUrl + '/admin/product_sections/'+objectId, {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'get'
            }).then((response) => {
                return response.json();
            });
            if (productSections.success) {
                dispatch({
                    type: 'SET_PRODUCT_SECTIONS',
                    data: productSections.data.productSections
                });
                dispatch({
                    type: 'MUST_GET_PRODUCT_SECTIONS',
                    data: []
                });
            }
        },
        getLanguages: async (companyId) => {
            let languages = await fetch(apiBaseUrl + '/admin/languages/' + companyId, {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'get'
            }).then((response) => {
                return response.json();
            });
            if (languages.success) {
                dispatch({
                    type: 'SET_LANGUAGES',
                    data: languages.data.languages
                });
            }
        },
        get: async (id) => {
            let productSection = await fetch(apiBaseUrl + '/admin/product_section/' + id, {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'get'
            }).then((response) => {
                return response.json();
            });
            if (productSection.success) {
                dispatch({
                    type: 'SET_PRODUCT_SECTION',
                    data: productSection.data.productSection
                });
                dispatch({
                    type: 'GET_PRODUCT_SECTION',
                    data: []
                });
            }
        },
        add: async (data) => {
            let productSection = await fetch(apiBaseUrl + '/admin/product_section/create', {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'post',
                body: JSON.stringify(data)
            }).then((response) => {
                return response.json();
            });
            if (productSection.success) {
                dispatch({
                    type: 'SET_PRODUCT_SECTION',
                    data: productSection.data.productSection
                });
                dispatch({
                    type: 'PRODUCT_SECTION_CREATED',
                    data: []
                });
            } else {
                dispatch({
                    type: 'CREATE_PRODUCT_SECTION_ERRORS',
                    data: productSection.errors
                });
            }
        },
        edit: async (id, data) => {
            let productSection = await fetch(apiBaseUrl + '/admin/product_section/edit/' + id, {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'post',
                body: JSON.stringify(data)
            }).then((response) => {
                return response.json();
            });
            if (productSection.success) {
                dispatch({
                    type: 'SET_PRODUCT_SECTION',
                    data: productSection.data.productSection
                });
                dispatch({
                    type: 'PRODUCT_SECTION_EDITED',
                    data: []
                });
            } else {
                dispatch({
                    type: 'EDIT_PRODUCT_SECTION_ERRORS',
                    data: productSection.errors
                });
            }
        },
        delete: async (id) => {
            let productSection = await fetch(apiBaseUrl + '/admin/product_section/delete/' + id, {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'get'
            }).then((response) => {
                return response.json();
            });
        },
        setDefaults: async () => {
            dispatch({
                type: 'UNSET_PRODUCT_SECTION_GET',
                data: []
            });
            dispatch({
                type: 'REMOVE_CREATE_PRODUCT_SECTION_ERRORS',
                data: []
            });
            dispatch({
                type: 'REMOVE_EDIT_PRODUCT_SECTION_ERRORS',
                data: []
            });
            dispatch({
                type: 'UNSET_PRODUCT_SECTION_CREATED',
                data: []
            });
            dispatch({
                type: 'UNSET_PRODUCT_SECTION_EDITED',
                data: []
            });
            dispatch({
                type: 'UNSET_MUST_GET_PRODUCT_SECTIONS',
                data: []
            });
        },
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductSections);