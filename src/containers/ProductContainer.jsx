import React from 'react'
import {connect} from 'react-redux'
import apiBaseUrl from './../AppConfig.jsx';
import cookie from 'react-cookies';
import Product from "../views/Product.jsx";


let Products = (props) => {
    return (
        <Product {...props}/>
    )
};

const mapStateToProps = (state, ownProps) => {
    return state.ProductReducer.toJS();
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        all: async (sectionId) => {
            let products = await fetch(apiBaseUrl + '/admin/products/' + sectionId, {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'get'
            }).then((response) => {
                return response.json();
            });
            if (products.success) {
                dispatch({
                    type: 'SET_PRODUCTS',
                    data: products.data.products
                });
                dispatch({
                    type: 'MUST_GET_PRODUCTS',
                    data: []
                });
            }
        },
        getLanguages: async (companyId) => {
            let languages = await fetch(apiBaseUrl + '/admin/languages/' + companyId, {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'get'
            }).then((response) => {
                return response.json();
            });
            if (languages.success) {
                dispatch({
                    type: 'SET_LANGUAGES',
                    data: languages.data.languages
                });
            }
        },
        get: async (id) => {
            let product = await fetch(apiBaseUrl + '/admin/product/' + id, {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'get'
            }).then((response) => {
                return response.json();
            });
            if (product.success) {
                dispatch({
                    type: 'SET_PRODUCT',
                    data: product.data.product
                });
                dispatch({
                    type: 'GET_PRODUCT',
                    data: []
                });
            }
        },
        add: async (sectionId, names, image, price, discount) => {
            let product = await fetch(apiBaseUrl + '/admin/product/create', {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'post',
                body: JSON.stringify({
                    section_id: sectionId,
                    names: names,
                    image: image,
                    price: price,
                    discount: discount
                })
            }).then((response) => {
                return response.json();
            });
            if (product.success) {
                dispatch({
                    type: 'SET_PRODUCT',
                    data: product.data.product
                });
                dispatch({
                    type: 'PRODUCT_CREATED',
                    data: []
                });
            } else {
                dispatch({
                    type: 'CREATE_PRODUCT_ERRORS',
                    data: product.errors
                });
            }
        },
        edit: async (id, sectionId, names, image, price, discount) => {
            let product = await fetch(apiBaseUrl + '/admin/product/edit/' + id, {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'post',
                body: JSON.stringify({
                    section_id: sectionId,
                    names: names,
                    image: image,
                    price: price,
                    discount: discount
                })
            }).then((response) => {
                return response.json();
            });
            if (product.success) {
                dispatch({
                    type: 'SET_PRODUCT',
                    data: product.data.product
                });
                dispatch({
                    type: 'PRODUCT_EDITED',
                    data: []
                });
            } else {
                dispatch({
                    type: 'EDIT_PRODUCT_ERRORS',
                    data: product.errors
                });
            }
        },
        delete: async (id) => {
            let product = await fetch(apiBaseUrl + '/admin/product/delete/' + id, {
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + cookie.load('token')
                },
                method: 'get'
            }).then((response) => {
                return response.json();
            });
        },
        getImageBase64: async (imageUrl) => {
            let image = await fetch(imageUrl).then((response) => {
                response.arrayBuffer().then((buffer) => {
                    var base64Flag = 'data:image/jpeg;base64,';
                    var imageStr = arrayBufferToBase64(buffer);
                    dispatch({
                        type: 'SET_IMAGE',
                        data: base64Flag+imageStr
                    });
                })
            });
        },
        setDefaults: async () => {
            dispatch({
                type: 'UNSET_PRODUCT_GET',
                data: []
            });
            dispatch({
                type: 'REMOVE_CREATE_PRODUCT_ERRORS',
                data: []
            });
            dispatch({
                type: 'UNSET_IMAGE',
                data: []
            });
            dispatch({
                type: 'REMOVE_EDIT_PRODUCT_ERRORS',
                data: []
            });
            dispatch({
                type: 'UNSET_PRODUCT_CREATED',
                data: []
            });
            dispatch({
                type: 'UNSET_PRODUCT_EDITED',
                data: []
            });
            dispatch({
                type: 'UNSET_MUST_GET_PRODUCTS',
                data: []
            });
        },
    }
};
function arrayBufferToBase64(buffer) {
    var binary = '';
    var bytes = [].slice.call(new Uint8Array(buffer));

    bytes.forEach((b) => binary += String.fromCharCode(b));

    return window.btoa(binary);
};
export default connect(mapStateToProps, mapDispatchToProps)(Products);