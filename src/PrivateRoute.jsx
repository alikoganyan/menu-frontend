import React from 'react'
import {Redirect, Route} from 'react-router-dom';
import cookie from 'react-cookies'

const PrivateRoute = ({component: Component, ...rest}) => {
    let isLoggedIn=false;
    if(cookie.load('token')) {
        isLoggedIn=true;
    }
    return (
        <Route
            {...rest}
            render={props =>
                isLoggedIn ? (
                    <Component {...props} />
                ) : (
                    <Redirect to={{pathname: '/login', state: {from: props.location}}}/>
                )
            }
        />
    )
};

export default PrivateRoute