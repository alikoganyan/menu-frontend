import {combineReducers} from 'redux'
import LoginReducer from './LoginReducer.jsx';
import UserReducer from './UserReducer.jsx';
import CompanyReducer from './CompanyReducer.jsx';
import CompanyUserReducer from './CompanyUserReducer.jsx';
import ShopReducer from './ShopReducer.jsx';
import RestaurantReducer from './RestaurantReducer.jsx';
import PharmacyReducer from './PharmacyReducer.jsx';
import LanguageReducer from './LanguageReducer.jsx';
import DefaultTranslationReducer from './DefaultTranslationReducer.jsx';
import CategoryReducer from './CategoryReducer.jsx';
import RestaurantTypeReducer from './RestaurantTypeReducer.jsx';
import ProductSectionReducer from './ProductSectionReducer.jsx';
import ProductReducer from './ProductReducer.jsx';
import OrderReducer from './OrderReducer.jsx';

const app = combineReducers({
    LoginReducer,
    UserReducer,
    CompanyReducer,
    CompanyUserReducer,
    ShopReducer,
    RestaurantReducer,
    PharmacyReducer,
    LanguageReducer,
    DefaultTranslationReducer,
    CategoryReducer,
    RestaurantTypeReducer,
    ProductSectionReducer,
    ProductReducer,
    OrderReducer,
});

export default app