import {Map} from 'immutable';

const ShopReducer = (state = Map({
    shops: '',
    managers: '',
    mustGetShops: '',
    mustGetManagers: '',
    categories: '',
    shop: '',
    shopCreated: '',
    createShopErrors: '',
    shopEdited: '',
    editShopErrors: '',
    shopGet: '',
    unsetShopGet: '',
}), action) => {
    switch (action.type) {
        case 'SET_SHOPS':
            return state.set('shops', action.data);
        case 'SET_MANAGERS':
            return state.set('managers', action.data);
        case 'SET_CATEGORIES':
            return state.set('categories', action.data);
        case 'MUST_GET_SHOPS':
            return state.set('mustGetShops', true);
        case 'MUST_GET_MANAGERS':
            return state.set('mustGetManagers', true);
        case 'UNSET_MUST_GET_SHOPS':
            return state.set('mustGetShops', false);
        case 'UNSET_MUST_GET_MANAGERS':
            return state.set('mustGetManagers', false);
        case 'MUST_GET_CATEGORIES':
            return state.set('mustGetCategories', true);
        case 'UNSET_MUST_GET_CATEGORIES':
            return state.set('mustGetCategories', false);
        case 'SET_SHOP':
            return state.set('shop', action.data);
        case 'SHOP_CREATED':
            return state.set('shopCreated', true);
        case 'UNSET_SHOP_CREATED':
            return state.set('shopCreated', false);
        case 'CREATE_SHOP_ERRORS':
            return state.set('createShopErrors', action.data);
        case 'REMOVE_CREATE_SHOP_ERRORS':
            return state.set('createShopErrors', []);
        case 'SHOP_EDITED':
            return state.set('shopEdited', true);
        case 'UNSET_SHOP_EDITED':
            return state.set('shopEdited', false);
        case 'EDIT_SHOP_ERRORS':
            return state.set('editShopErrors', action.data);
        case 'REMOVE_EDIT_SHOP_ERRORS':
            return state.set('editShopErrors', []);
        case 'GET_SHOP':
            return state.set('shopGet', true);
        case 'UNSET_SHOP_GET':
            return state.set('shopGet', false);
        default:
            return state
    }
};

export default ShopReducer