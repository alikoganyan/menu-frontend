import {Map} from 'immutable';

const LanguageReducer = (state = Map({
    languages: '',
    mustGetLanguages: '',
    language: '',
    languageCreated: '',
    createLanguageErrors: '',
    languageEdited: '',
    editLanguageErrors: '',
    languageGet: '',
    unsetLanguageGet: '',
}), action) => {
    switch (action.type) {
        case 'SET_LANGUAGES':
            return state.set('languages', action.data);
        case 'MUST_GET_LANGUAGES':
            return state.set('mustGetLanguages', true);
        case 'UNSET_MUST_GET_LANGUAGES':
            return state.set('mustGetLanguages', false);
        case 'SET_LANGUAGE':
            return state.set('language', action.data);
        case 'LANGUAGE_CREATED':
            return state.set('languageCreated', true);
        case 'UNSET_LANGUAGE_CREATED':
            return state.set('languageCreated', false);
        case 'CREATE_LANGUAGE_ERRORS':
            return state.set('createLanguageErrors', action.data);
        case 'REMOVE_CREATE_LANGUAGE_ERRORS':
            return state.set('createLanguageErrors', []);
        case 'LANGUAGE_EDITED':
            return state.set('languageEdited', true);
        case 'UNSET_LANGUAGE_EDITED':
            return state.set('languageEdited', false);
        case 'EDIT_LANGUAGE_ERRORS':
            return state.set('editLanguageErrors', action.data);
        case 'REMOVE_EDIT_LANGUAGE_ERRORS':
            return state.set('editLanguageErrors', []);
        case 'GET_LANGUAGE':
            return state.set('languageGet', true);
        case 'UNSET_LANGUAGE_GET':
            return state.set('languageGet', false);
        default:
            return state
    }
};

export default LanguageReducer