import {Map} from 'immutable';

const RestaurantReducer = (state = Map({
    restaurants: '',
    restaurantTypes: '',
    mustGetRestaurants: '',
    mustGetManagers: '',
    mustGetRestaurantTypes: '',
    restaurant: '',
    restaurantCreated: '',
    createRestaurantErrors: '',
    restaurantEdited: '',
    editRestaurantErrors: '',
    restaurantGet: '',
    unsetRestaurantGet: '',
}), action) => {
    switch (action.type) {
        case 'SET_RESTAURANTS':
            return state.set('restaurants', action.data);
        case 'SET_MANAGERS':
            return state.set('managers', action.data);
        case 'SET_RESTAURANT_TYPES':
            return state.set('restaurantTypes', action.data);
        case 'MUST_GET_RESTAURANTS':
            return state.set('mustGetRestaurants', true);
        case 'MUST_GET_MANAGERS':
            return state.set('mustGetManagers', true);
        case 'UNSET_MUST_GET_RESTAURANTS':
            return state.set('mustGetRestaurants', false);
        case 'UNSET_MUST_GET_MANAGERS':
            return state.set('mustGetManagers', false);
        case 'MUST_GET_RESTAURANT_TYPES':
            return state.set('mustGetRestaurantTypes', true);
        case 'UNSET_MUST_GET_RESTAURANT_TYPES':
            return state.set('mustGetRestaurantTypes', false);
        case 'SET_RESTAURANT':
            return state.set('restaurant', action.data);
        case 'RESTAURANT_CREATED':
            return state.set('restaurantCreated', true);
        case 'UNSET_RESTAURANT_CREATED':
            return state.set('restaurantCreated', false);
        case 'CREATE_RESTAURANT_ERRORS':
            return state.set('createRestaurantErrors', action.data);
        case 'REMOVE_CREATE_RESTAURANT_ERRORS':
            return state.set('createRestaurantErrors', []);
        case 'RESTAURANT_EDITED':
            return state.set('restaurantEdited', true);
        case 'UNSET_RESTAURANT_EDITED':
            return state.set('restaurantEdited', false);
        case 'EDIT_RESTAURANT_ERRORS':
            return state.set('editRestaurantErrors', action.data);
        case 'REMOVE_EDIT_RESTAURANT_ERRORS':
            return state.set('editRestaurantErrors', []);
        case 'GET_RESTAURANT':
            return state.set('restaurantGet', true);
        case 'UNSET_RESTAURANT_GET':
            return state.set('restaurantGet', false);
        default:
            return state
    }
};

export default RestaurantReducer