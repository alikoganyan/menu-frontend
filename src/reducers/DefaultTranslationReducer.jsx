import {Map} from 'immutable';

const DefaultTranslationReducer = (state = Map({
    defaultTranslations: '',
    mustGetDefaultTranslations: '',
    defaultTranslationEdited: '',
}), action) => {
    switch (action.type) {
        case 'SET_DEFAULT_TRANSLATIONS':
            return state.set('defaultTranslations', action.data);
        case 'MUST_GET_DEFAULT_TRANSLATIONS':
            return state.set('mustGetDefaultTranslations', true);
        case 'UNSET_MUST_GET_DEFAULT_TRANSLATIONS':
            return state.set('mustGetDefaultTranslations', false);
        case 'DEFAULT_TRANSLATION_EDITED':
            return state.set('defaultTranslationEdited', true);
        case 'UNSET_DEFAULT_TRANSLATION_EDITED':
            return state.set('defaultTranslationEdited', false);
        default:
            return state
    }
};

export default DefaultTranslationReducer