import {Map} from 'immutable';

const RestaurantTypeReducer = (state = Map({
    restaurantTypes: '',
    languages: '',
    mustGetRestaurantTypes: '',
    restaurantType: '',
    restaurantTypeCreated: '',
    createRestaurantTypeErrors: '',
    restaurantTypeEdited: '',
    editRestaurantTypeErrors: '',
    restaurantTypeGet: '',
    unsetRestaurantTypeGet: '',
}), action) => {
    switch (action.type) {
        case 'SET_RESTAURANT_TYPES':
            return state.set('restaurantTypes', action.data);
        case 'SET_LANGUAGES':
            return state.set('languages', action.data);
        case 'MUST_GET_RESTAURANT_TYPES':
            return state.set('mustGetRestaurantTypes', true);
        case 'UNSET_MUST_GET_RESTAURANT_TYPES':
            return state.set('mustGetRestaurantTypes', false);
        case 'SET_RESTAURANT_TYPE':
            return state.set('restaurantType', action.data);
        case 'RESTAURANT_TYPE_CREATED':
            return state.set('restaurantTypeCreated', true);
        case 'UNSET_RESTAURANT_TYPE_CREATED':
            return state.set('restaurantTypeCreated', false);
        case 'CREATE_RESTAURANT_TYPE_ERRORS':
            return state.set('createRestaurantTypeErrors', action.data);
        case 'REMOVE_CREATE_RESTAURANT_TYPE_ERRORS':
            return state.set('createRestaurantTypeErrors', []);
        case 'RESTAURANT_TYPE_EDITED':
            return state.set('restaurantTypeEdited', true);
        case 'UNSET_RESTAURANT_TYPE_EDITED':
            return state.set('restaurantTypeEdited', false);
        case 'EDIT_RESTAURANT_TYPE_ERRORS':
            return state.set('editRestaurantTypeErrors', action.data);
        case 'REMOVE_EDIT_RESTAURANT_TYPE_ERRORS':
            return state.set('editRestaurantTypeErrors', []);
        case 'GET_RESTAURANT_TYPE':
            return state.set('restaurantTypeGet', true);
        case 'UNSET_RESTAURANT_TYPE_GET':
            return state.set('restaurantTypeGet', false);
        default:
            return state
    }
};

export default RestaurantTypeReducer