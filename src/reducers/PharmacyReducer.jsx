import {Map} from 'immutable';

const PharmacyReducer = (state = Map({
    pharmacies: '',
    mustGetPharmacies: '',
    pharmacy: '',
    pharmacyCreated: '',
    createPharmacyErrors: '',
    pharmacyEdited: '',
    editPharmacyErrors: '',
    pharmacyGet: '',
    unsetPharmacyGet: '',
}), action) => {
    switch (action.type) {
        case 'SET_PHARMACIES':
            return state.set('pharmacies', action.data);
        case 'SET_MANAGERS':
            return state.set('managers', action.data);
        case 'MUST_GET_PHARMACIES':
            return state.set('mustGetPharmacies', true);
        case 'MUST_GET_MANAGERS':
            return state.set('mustGetManagers', true);
        case 'UNSET_MUST_GET_PHARMACIES':
            return state.set('mustGetPharmacies', false);
        case 'UNSET_MUST_GET_MANAGERS':
            return state.set('mustGetManagers', false);
        case 'SET_PHARMACY':
            return state.set('pharmacy', action.data);
        case 'PHARMACY_CREATED':
            return state.set('pharmacyCreated', true);
        case 'UNSET_PHARMACY_CREATED':
            return state.set('pharmacyCreated', false);
        case 'CREATE_PHARMACY_ERRORS':
            return state.set('createPharmacyErrors', action.data);
        case 'REMOVE_CREATE_PHARMACY_ERRORS':
            return state.set('createPharmacyErrors', []);
        case 'PHARMACY_EDITED':
            return state.set('pharmacyEdited', true);
        case 'UNSET_PHARMACY_EDITED':
            return state.set('pharmacyEdited', false);
        case 'EDIT_PHARMACY_ERRORS':
            return state.set('editPharmacyErrors', action.data);
        case 'REMOVE_EDIT_PHARMACY_ERRORS':
            return state.set('editPharmacyErrors', []);
        case 'GET_PHARMACY':
            return state.set('pharmacyGet', true);
        case 'UNSET_PHARMACY_GET':
            return state.set('pharmacyGet', false);
        default:
            return state
    }
};

export default PharmacyReducer