import {Map} from 'immutable';

const ProductReducer = (state = Map({
    products: '',
    languages: '',
    image: '',
    mustGetProducts: '',
    product: '',
    productCreated: '',
    createProductErrors: '',
    productEdited: '',
    editProductErrors: '',
    productGet: '',
    unsetProductGet: '',
}), action) => {
    switch (action.type) {
        case 'SET_PRODUCTS':
            return state.set('products', action.data);
        case 'SET_LANGUAGES':
            return state.set('languages', action.data);
        case 'SET_IMAGE':
            return state.set('image', action.data);
        case 'UNSET_IMAGE':
            return state.set('image', '');
        case 'MUST_GET_PRODUCTS':
            return state.set('mustGetProducts', true);
        case 'UNSET_MUST_GET_PRODUCTS':
            return state.set('mustGetProducts', false);
        case 'SET_PRODUCT':
            return state.set('product', action.data);
        case 'PRODUCT_CREATED':
            return state.set('productCreated', true);
        case 'UNSET_PRODUCT_CREATED':
            return state.set('productCreated', false);
        case 'CREATE_PRODUCT_ERRORS':
            return state.set('createProductErrors', action.data);
        case 'REMOVE_CREATE_PRODUCT_ERRORS':
            return state.set('createProductErrors', []);
        case 'PRODUCT_EDITED':
            return state.set('productEdited', true);
        case 'UNSET_PRODUCT_EDITED':
            return state.set('productEdited', false);
        case 'EDIT_PRODUCT_ERRORS':
            return state.set('editProductErrors', action.data);
        case 'REMOVE_EDIT_PRODUCT_ERRORS':
            return state.set('editProductErrors', []);
        case 'GET_PRODUCT':
            return state.set('productGet', true);
        case 'UNSET_PRODUCT_GET':
            return state.set('productGet', false);
        default:
            return state
    }
};

export default ProductReducer