import {Map} from 'immutable';

const CategoryReducer = (state = Map({
    categories: '',
    languages: '',
    mustGetCategories: '',
    category: '',
    categoryCreated: '',
    createCategoryErrors: '',
    categoryEdited: '',
    editCategoryErrors: '',
    categoryGet: '',
    unsetCategoryGet: '',
}), action) => {
    switch (action.type) {
        case 'SET_CATEGORIES':
            return state.set('categories', action.data);
        case 'SET_LANGUAGES':
            return state.set('languages', action.data);
        case 'MUST_GET_CATEGORIES':
            return state.set('mustGetCategories', true);
        case 'UNSET_MUST_GET_CATEGORIES':
            return state.set('mustGetCategories', false);
        case 'SET_CATEGORY':
            return state.set('category', action.data);
        case 'CATEGORY_CREATED':
            return state.set('categoryCreated', true);
        case 'UNSET_CATEGORY_CREATED':
            return state.set('categoryCreated', false);
        case 'CREATE_CATEGORY_ERRORS':
            return state.set('createCategoryErrors', action.data);
        case 'REMOVE_CREATE_CATEGORY_ERRORS':
            return state.set('createCategoryErrors', []);
        case 'CATEGORY_EDITED':
            return state.set('categoryEdited', true);
        case 'UNSET_CATEGORY_EDITED':
            return state.set('categoryEdited', false);
        case 'EDIT_CATEGORY_ERRORS':
            return state.set('editCategoryErrors', action.data);
        case 'REMOVE_EDIT_CATEGORY_ERRORS':
            return state.set('editCategoryErrors', []);
        case 'GET_CATEGORY':
            return state.set('categoryGet', true);
        case 'UNSET_CATEGORY_GET':
            return state.set('categoryGet', false);
        default:
            return state
    }
};

export default CategoryReducer