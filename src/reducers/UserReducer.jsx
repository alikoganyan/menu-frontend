import {Map} from 'immutable';

const UserReducer = (state = Map({
    user: Map()
}), action) => {
    switch (action.type) {
        case 'GET_USER':
            return state.set('user', action.data);
        default:
            return state
    }
};

export default UserReducer