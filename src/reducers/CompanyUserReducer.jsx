import {Map} from 'immutable';

const CompanyUserReducer = (state = Map({
    companyUsers: '',
    mustGetCompanyUsers: '',
    companyUser: '',
    companyUserCreated: '',
    createCompanyUserErrors: '',
    companyUserEdited: '',
    editCompanyUserErrors: '',
    companyUserGet: '',
    unsetCompanyUserGet: '',
}), action) => {
    switch (action.type) {
        case 'SET_COMPANY_USERS':
            return state.set('companyUsers', action.data);
        case 'MUST_GET_COMPANY_USERS':
            return state.set('mustGetCompanyUsers', true);
        case 'UNSET_MUST_GET_COMPANY_USERS':
            return state.set('mustGetCompanyUsers', false);
        case 'SET_COMPANY_USER':
            return state.set('companyUser', action.data);
        case 'COMPANY_USER_CREATED':
            return state.set('companyUserCreated', true);
        case 'UNSET_COMPANY_USER_CREATED':
            return state.set('companyUserCreated', false);
        case 'CREATE_COMPANY_USER_ERRORS':
            return state.set('createCompanyUserErrors', action.data);
        case 'REMOVE_CREATE_COMPANY_USER_ERRORS':
            return state.set('createCompanyUserErrors', []);
        case 'COMPANY_USER_EDITED':
            return state.set('companyUserEdited', true);
        case 'UNSET_COMPANY_USER_EDITED':
            return state.set('companyUserEdited', false);
        case 'EDIT_COMPANY_USER_ERRORS':
            return state.set('editCompanyUserErrors', action.data);
        case 'REMOVE_EDIT_COMPANY_USER_ERRORS':
            return state.set('editCompanyUserErrors', []);
        case 'GET_COMPANY_USER':
            return state.set('companyUserGet', true);
        case 'UNSET_COMPANY_USER_GET':
            return state.set('companyUserGet', false);
        default:
            return state
    }
};

export default CompanyUserReducer