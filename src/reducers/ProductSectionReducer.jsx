import {Map} from 'immutable';

const ProductSectionReducer = (state = Map({
    productSections: '',
    languages: '',
    mustGetProductSections: '',
    productSection: '',
    productSectionCreated: '',
    createProductSectionErrors: '',
    productSectionEdited: '',
    editProductSectionErrors: '',
    productSectionGet: '',
    unsetProductSectionGet: '',
}), action) => {
    switch (action.type) {
        case 'SET_PRODUCT_SECTIONS':
            return state.set('productSections', action.data);
        case 'SET_LANGUAGES':
            return state.set('languages', action.data);
        case 'MUST_GET_PRODUCT_SECTIONS':
            return state.set('mustGetProductSections', true);
        case 'UNSET_MUST_GET_PRODUCT_SECTIONS':
            return state.set('mustGetProductSections', false);
        case 'SET_PRODUCT_SECTION':
            return state.set('productSection', action.data);
        case 'PRODUCT_SECTION_CREATED':
            return state.set('productSectionCreated', true);
        case 'UNSET_PRODUCT_SECTION_CREATED':
            return state.set('productSectionCreated', false);
        case 'CREATE_PRODUCT_SECTION_ERRORS':
            return state.set('createProductSectionErrors', action.data);
        case 'REMOVE_CREATE_PRODUCT_SECTION_ERRORS':
            return state.set('createProductSectionErrors', []);
        case 'PRODUCT_SECTION_EDITED':
            return state.set('productSectionEdited', true);
        case 'UNSET_PRODUCT_SECTION_EDITED':
            return state.set('productSectionEdited', false);
        case 'EDIT_PRODUCT_SECTION_ERRORS':
            return state.set('editProductSectionErrors', action.data);
        case 'REMOVE_EDIT_PRODUCT_SECTION_ERRORS':
            return state.set('editProductSectionErrors', []);
        case 'GET_PRODUCT_SECTION':
            return state.set('productSectionGet', true);
        case 'UNSET_PRODUCT_SECTION_GET':
            return state.set('productSectionGet', false);
        default:
            return state
    }
};

export default ProductSectionReducer