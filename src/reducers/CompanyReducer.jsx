import {Map} from 'immutable';

const CompanyReducer = (state = Map({
    companies: '',
    mustGetCompanies: '',
    company: '',
    companyCreated: '',
    createCompanyErrors: '',
    companyEdited: '',
    editCompanyErrors: '',
    companyGet: '',
    unsetCompanyGet: '',
}), action) => {
    switch (action.type) {
        case 'SET_COMPANIES':
            return state.set('companies', action.data);
        case 'MUST_GET_COMPANIES':
            return state.set('mustGetCompanies', true);
        case 'UNSET_MUST_GET_COMPANIES':
            return state.set('mustGetCompanies', false);
        case 'SET_COMPANY':
            return state.set('company', action.data);
        case 'COMPANY_CREATED':
            return state.set('companyCreated', true);
        case 'UNSET_COMPANY_CREATED':
            return state.set('companyCreated', false);
        case 'CREATE_COMPANY_ERRORS':
            return state.set('createCompanyErrors', action.data);
        case 'REMOVE_CREATE_COMPANY_ERRORS':
            return state.set('createCompanyErrors', []);
        case 'COMPANY_EDITED':
            return state.set('companyEdited', true);
        case 'UNSET_COMPANY_EDITED':
            return state.set('companyEdited', false);
        case 'EDIT_COMPANY_ERRORS':
            return state.set('editCompanyErrors', action.data);
        case 'REMOVE_EDIT_COMPANY_ERRORS':
            return state.set('editCompanyErrors', []);
        case 'GET_COMPANY':
            return state.set('companyGet', true);
        case 'UNSET_COMPANY_GET':
            return state.set('companyGet', false);
        default:
            return state
    }
};

export default CompanyReducer