import {Map} from 'immutable';

const LoginReducer = (state = Map({
    loginErrors: Map(),
    loginSuccess: ''
}), action) => {
    switch (action.type) {
        case 'LOGIN_ADD_ERRORS':
            return state.set('loginErrors', Map(action.data));
        case 'LOGIN_SUCCESS':
            return state.set('loginSuccess', true);
        default:
            return state
    }
};

export default LoginReducer