import {Map} from 'immutable';

const OrderReducer = (state = Map({
    orders: '',
    orderCancel: '',
    drivers: '',
    mustGetOrders: '',
    order: '',
    orderCreated: '',
    createOrderErrors: '',
    connectDriverErrors: '',
    orderEdited: '',
    orderCancelled: '',
    connectOrderErrors: '',
    orderGet: '',
    driversGet: '',
    unsetOrderGet: '',
    unsetDriversGet: '',
}), action) => {
    switch (action.type) {
        case 'SET_ORDERS':
            return state.set('orders', action.data);
        case 'ORDER_CANCEL':
            return state.set('orderCancel', action.data);
        case 'MUST_GET_ORDERS':
            return state.set('mustGetOrders', true);
        case 'UNSET_MUST_GET_ORDERS':
            return state.set('mustGetOrders', false);
        case 'SET_ORDER':
            return state.set('order', action.data);
        case 'SET_DRIVERS':
            return state.set('drivers', action.data);
        case 'ORDER_CREATED':
            return state.set('orderCreated', true);
        case 'UNSET_ORDER_CREATED':
            return state.set('orderCreated', false);
        case 'CREATE_ORDER_ERRORS':
            return state.set('createOrderErrors', action.data);
        case 'REMOVE_CREATE_ORDER_ERRORS':
            return state.set('createOrderErrors', []);
        case 'ORDER_EDITED':
            return state.set('orderEdited', true);
        case 'UNSET_ORDER_EDITED':
            return state.set('orderEdited', false);
        case 'ORDER_CANCELLED':
            return state.set('orderCancelled', true);
        case 'UNSET_ORDER_CANCELLED':
            return state.set('orderCancelled', false);
        case 'EDIT_ORDER_ERRORS':
            return state.set('connectOrderErrors', action.data);
        case 'REMOVE_EDIT_ORDER_ERRORS':
            return state.set('connectOrderErrors', []);
        case 'GET_ORDER':
            return state.set('orderGet', true);
        case 'GET_DRIVERS':
            return state.set('driversGet', true);
        // case 'SET_ORDER_CANCEL':
        //     return state.set('orderCancel', action.data);
        case 'UNSET_ORDER_GET':
            return state.set('orderGet', false);
        // case 'UNSET_ORDER_CANCEL':
        //     return state.set('orderCancel', false);
        case 'UNSET_DRIVERS_GET':
            return state.set('driversGet', false);
        default:
            return state
    }
};

export default OrderReducer