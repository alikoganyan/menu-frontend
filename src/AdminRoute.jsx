import React from 'react'
import {Redirect, Route} from 'react-router-dom';
import cookie from 'react-cookies'

const AdminRoute = ({component: Component, ...rest}) => {
    let isLoggedIn = false;
    if (cookie.load('token') && cookie.load('user').role_id == 1) {
        isLoggedIn = true;
    }
    return (
        <Route
            {...rest}
            render={props =>
                isLoggedIn ? (
                    <Component {...props} />
                ) : (
                    <Redirect to={{pathname: '/login', state: {from: props.location}}}/>
                )
            }
        />
    )
};

export default AdminRoute