import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {createStore} from 'redux';
import {HashRouter, Switch} from 'react-router-dom';
import {
    Route
} from 'react-router-dom';
import Login from './containers/LoginContainer.jsx';
import Logout from './components/Logout.jsx';
import {createBrowserHistory} from 'history';

import app from './reducers/index.jsx';
import 'simple-line-icons/css/simple-line-icons.css';
import '../scss/style.scss';
import "babel-polyfill";
import Full from './containers/Full.jsx';

const history = createBrowserHistory();

const store = createStore(app, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

const Root = ({store}) => (
    <Provider store={store}>
        <HashRouter history={history}>
            <Switch>
                <Route exact path="/login" name="Login Page" component={Login}/>
                <Route exact path="/logout" name="Logout Page" component={Logout}/>
                <Route path="/" name="Home" component={Full}/>
            </Switch>
        </HashRouter>
    </Provider>
);
ReactDOM.render((<Provider store={store}>
    <HashRouter history={history}>
        <Switch>
            <Route exact path="/login" name="Login Page" component={Login}/>
            <Route exact path="/logout" name="Logout Page" component={Logout}/>
            <Route path="/" name="Home" component={Full}/>
        </Switch>
    </HashRouter>
</Provider>), document.getElementById('root'));
