import React, {Component} from 'react';
import cookie from "react-cookies";

class Header extends Component {

    constructor(props) {
        super(props);
        this.logout = this.logout.bind(this);
    }

    logout() {
        cookie.remove('token');
        this.props.history.push("login");
    }

    componentDidMount() {
        this.logout();
    }

    render() {
        return (
            <div></div>
        )
    }
}

export default Header;
