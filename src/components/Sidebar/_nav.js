export default {
    items: [
        {
            name: 'Dashboard',
            url: '/dashboard',
            icon: 'icon-speedometer',
            badge: {
                variant: 'info',
                text: 'NEW'
            }
        },
        {
            name: 'Companies',
            url: '/companies',
            icon: 'icon-speedometer',
            badge: {
                variant: '',
                text: ''
            }
        },
        {
            name: 'Shops',
            url: '/shops',
            icon: 'icon-speedometer',
            badge: {
                variant: '',
                text: ''
            }
        },
        {
            name: 'Restaurants',
            url: '/restaurants',
            icon: 'icon-speedometer',
            badge: {
                variant: '',
                text: ''
            }
        },
        {
            name: 'Pharmacies',
            url: '/pharmacies',
            icon: 'icon-speedometer',
            badge: {
                variant: '',
                text: ''
            }
        },
        {
            name: 'Languages',
            url: '/languages',
            icon: 'icon-speedometer',
            badge: {
                variant: '',
                text: ''
            }
        },
        {
            name: 'Categories',
            url: '/categories',
            icon: 'icon-speedometer',
            badge: {
                variant: '',
                text: ''
            }
        },
        {
            name: 'Restaurant Types',
            url: '/restaurant-types',
            icon: 'icon-speedometer',
            badge: {
                variant: '',
                text: ''
            }
        }
    ]
};
