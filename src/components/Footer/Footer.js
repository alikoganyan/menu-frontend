import React, { Component } from 'react';

class Footer extends Component {
  render() {
    return (
      <footer className="app-footer">
        <a href="http://coreui.io">Menu</a> &copy; 2018 owlfinity.
        <span className="float-right">Powered by <a href="http://coreui.io">OWLFinity</a></span>
      </footer>
    )
  }
}

export default Footer;
