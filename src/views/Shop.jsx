import React, {Component} from 'react';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import '../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css';
import {Button, Modal, ModalHeader, ModalBody, ModalFooter} from "reactstrap";
import classNames from 'classnames';
import {List} from 'immutable';
import PlacesAutocomplete, {geocodeByAddress, getLatLng} from 'react-places-autocomplete'
import {Link} from "react-router-dom";
import cookie from "react-cookies";

let shops = [];
let categories = [];
let managers = [];

class Shop extends Component {
    constructor(props) {
        super(props);
        this.state = {
            options: {
                sizePerPage: 10,
                hideSizePerPage: true
            },
            shopId: 0,
            createModal: false,
            editModal: false,
            deleteModal: false,
            createShopAddressName: '',
            editShopAddressName: ''
        };
        this.changeCreateShopAddressInput = (createShopAddressName) => this.setState({createShopAddressName});
        this.changeEditShopAddressInput = (editShopAddressName) => this.setState({editShopAddressName});
        this.all = this.all.bind(this);
        this.get = this.get.bind(this);
        this.getCategories = this.getCategories.bind(this);
        this.add = this.add.bind(this);
        this.edit = this.edit.bind(this);
        this.delete = this.delete.bind(this);
        this.toggleCreateModal = this.toggleCreateModal.bind(this);
        this.toggleEditModal = this.toggleEditModal.bind(this);
        this.toggleDeleteModal = this.toggleDeleteModal.bind(this);
    }

    componentDidMount() {
        this.all();
        this.getCategories();
        if (cookie.load('user').role_id == 2) {
            this.props.getManagers(cookie.load('user').company_id);
        } else {
            this.props.getManagers();
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.mustGetShops) {
            shops = this.props.shops;
        }
        if (nextProps.categories) {
            categories = nextProps.categories;
        }
        if (nextProps.shopCreated) {
            shops = [this.props.shop, ...shops];
            this.toggleCreateModal();
            this.setState({
                createShopAddressName: ""
            });
            this.props.setDefaults();
        }
        if (nextProps.shopEdited) {
            this.toggleEditModal();
            const index = shops.findIndex(item => item.id == this.state.shopId);
            let shop = this.props.shop;
            shops = List(shops).update(index, function () {
                return shop;
            }).toArray();
            this.props.setDefaults();
        }
        if (nextProps.shopGet) {
            this.toggleEditModal();
            this.setState({
                shopId: this.props.shop.id,
                editShopAddressName: this.props.shop.address_name
            });
            this.props.setDefaults();
        }
        if (nextProps.managers) {
            managers = nextProps.managers.map((manager, key) =>
                <option key={key} value={manager.id}>{manager.name}</option>
            )
        }
    }

    toggleCreateModal() {
        this.setState({
            createModal: !this.state.createModal
        });
    }

    toggleEditModal() {
        this.setState({
            editModal: !this.state.editModal
        });
    }

    toggleDeleteModal(id) {
        if (id) {
            this.setState({
                shopId: id
            });
        }
        this.setState({
            deleteModal: !this.state.deleteModal
        });
    }

    actionFormatter(cell, row) {
        return (<div><Button color={'success'} onClick={() => this.get(row.id)}>
            Edit
        </Button>{' '}
            <Button color={'danger'} onClick={() => this.toggleDeleteModal(row.id)}>
                Delete
            </Button>{' '}
            <Link to={'/product-sections/' + row.id}><Button color={'success'}>
                <i className="fa fa-dropbox"></i><br/>
            </Button></Link>
        </div>);
    }

    categoryNameFormatter(cell, row) {
        return row.category.default_name;
    }

    managerNameFormatter(cell, row) {
        let managerName = "";
        if (row.manager) {
            managerName = row.manager.name;
        }
        return managerName;
    }

    all() {
        this.props.all();
    }

    get(id) {
        this.props.get(id);
        this.setState({shopId: id});
    }

    getCategories() {
        this.props.getCategories();
    }

    add(e) {
        e.preventDefault();
        let component = this;
        geocodeByAddress(this.state.createShopAddressName)
            .then(results => getLatLng(results[0]))
            .then(function (latLng) {
                component.props.add(component.name.value, component.phoneNumber.value, component.state.createShopAddressName, latLng.lat, latLng.lng, component.category.value, component.manager.value)
            })
            .catch(function (error) {
                component.props.add(component.name.value, component.phoneNumber.value, component.state.createShopAddressName, '', '', component.category.value, component.manager.value);
            });
    }

    edit(e) {
        e.preventDefault();
        let component = this;
        geocodeByAddress(this.state.editShopAddressName)
            .then(results => getLatLng(results[0]))
            .then(function (latLng) {
                component.props.edit(component.state.shopId, component.nameEdit.value, component.phoneNumberEdit.value, component.state.editShopAddressName, latLng.lat, latLng.lng, component.categoryEdit.value, component.managerEdit.value);
            })
            .catch(function (error) {
                component.props.edit(component.state.shopId, component.nameEdit.value, component.phoneNumberEdit.value, component.state.editShopAddressName, '', '', component.categoryEdit.value, component.managerEdit.value);
            });
    }

    delete() {
        this.props.delete(this.state.shopId);
        this.toggleDeleteModal();
        const index = shops.findIndex(item => item.id == this.state.shopId);
        shops = List(shops).delete(index).toArray();
    }

    render() {
        const createShopAddressNameInputProps = {
            value: this.state.createShopAddressName,
            placeholder: 'Search Address',
            onChange: this.changeCreateShopAddressInput,
        };
        const editShopAddressNameInputProps = {
            value: this.state.editShopAddressName,
            placeholder: 'Search Address',
            onChange: this.changeEditShopAddressInput,
        };
        const categoryOptions = categories.map((category, key) =>
            <option value={category.id} key={key}>{category.default_name}</option>
        );
        return (
            <div>
                {cookie.load('user').role_id == 2 || cookie.load('user').role_id == 3 ?
                    <Button color={'success'} onClick={this.toggleCreateModal} style={{'marginBottom': '20px'}}>Add
                        Shop</Button>
                    : ''}
                <Modal isOpen={this.state.createModal} toggle={this.toggleCreateModal}
                       className={this.props.className}>
                    <form onSubmit={this.add}>
                        <ModalHeader toggle={this.toggleCreateModal}>Add Shop</ModalHeader>
                        <ModalBody>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.createShopErrors.name})}>
                                <input type="text" placeholder="Name"
                                       className={classNames("form-control", {'is-invalid': this.props.createShopErrors.name})}
                                       ref={(input) => this.name = input}/>
                                {this.props.createShopErrors.name ? <div className={"invalid-feedback"}>
                                    {this.props.createShopErrors.name}
                                </div> : null}
                            </div>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.createShopErrors.phone_number})}>
                                <input type="text" placeholder="Phone Number"
                                       className={classNames("form-control", {'is-invalid': this.props.createShopErrors.phone_number})}
                                       ref={(input) => this.phoneNumber = input}/>
                                {this.props.createShopErrors.phone_number ? <div className={"invalid-feedback"}>
                                    {this.props.createShopErrors.phone_number}
                                </div> : null}
                            </div>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.createShopErrors.address_name})}>
                                <PlacesAutocomplete inputProps={createShopAddressNameInputProps}
                                                    classNames={{input: classNames("form-control", {'is-invalid': this.props.createShopErrors.address_name || this.props.createShopErrors.address_lat || this.props.createShopErrors.address_lng})}}/>
                                {this.props.createShopErrors.address_name || this.props.createShopErrors.address_lat || this.props.createShopErrors.address_lng ?
                                    <div className={"invalid-feedback"} style={{display: 'block'}}>
                                        {this.props.createShopErrors.address_name}
                                    </div> : null}
                            </div>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.createShopErrors.category_id})}>
                                <select
                                    className={classNames('form-control', {'is-invalid': this.props.createShopErrors.category_id})}
                                    ref={(input) => this.category = input}>
                                    <option value={''}>Select Category</option>
                                    {categoryOptions}
                                </select>
                                {this.props.createShopErrors.category_id ? <div className={"invalid-feedback"}>
                                    {this.props.createShopErrors.category_id}
                                </div> : null}
                            </div>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.createShopErrors.manager_id})}>
                                <select
                                    className={classNames('form-control', {'is-invalid': this.props.createShopErrors.manager_id})}
                                    ref={(input) => this.manager = input}>
                                    <option value={''}>Select Manager</option>
                                    {managers}
                                </select>
                                {this.props.createShopErrors.manager_id ? <div className={"invalid-feedback"}>
                                    {this.props.createShopErrors.manager_id}
                                </div> : null}
                            </div>
                        </ModalBody>
                        <ModalFooter>
                            <Button color="primary">Add</Button>{' '}
                            <Button color="secondary" onClick={this.toggleCreateModal}>Cancel</Button>
                        </ModalFooter>
                    </form>
                </Modal>

                <Modal isOpen={this.state.editModal} toggle={this.toggleEditModal} className={this.props.className}>
                    <form onSubmit={this.edit}>
                        <ModalHeader toggle={this.toggleEditModal}>Edit Shop</ModalHeader>
                        <ModalBody>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.editShopErrors.name})}>
                                <input type="text" placeholder="Name"
                                       className={classNames("form-control", {'is-invalid': this.props.editShopErrors.name})}
                                       ref={(input) => this.nameEdit = input}
                                       defaultValue={this.props.shop ? this.props.shop.name : ''}/>
                                {this.props.editShopErrors.name ? <div className={"invalid-feedback"}>
                                    {this.props.editShopErrors.name}
                                </div> : null}
                            </div>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.editShopErrors.phone_number})}>
                                <input type="text" placeholder="Phone Number"
                                       className={classNames("form-control", {'is-invalid': this.props.editShopErrors.phone_number})}
                                       ref={(input) => this.phoneNumberEdit = input}
                                       defaultValue={this.props.shop ? this.props.shop.phone_number : ''}/>
                                {this.props.editShopErrors.phone_number ? <div className={"invalid-feedback"}>
                                    {this.props.editShopErrors.phone_number}
                                </div> : null}
                            </div>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.editShopErrors.address_name})}>
                                <PlacesAutocomplete inputProps={editShopAddressNameInputProps}
                                                    classNames={{input: classNames("form-control", {'is-invalid': this.props.editShopErrors.address_name || this.props.editShopErrors.address_lat || this.props.editShopErrors.address_lng})}}/>
                                {this.props.editShopErrors.address_name || this.props.editShopErrors.address_lat || this.props.editShopErrors.address_lng ?
                                    <div className={"invalid-feedback"} style={{display: 'block'}}>
                                        {this.props.editShopErrors.address_name}
                                    </div> : null}
                            </div>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.editShopErrors.category_id})}>
                                <select
                                    className={classNames('form-control', {'is-invalid': this.props.editShopErrors.category_id})}
                                    ref={(input) => this.categoryEdit = input}
                                    defaultValue={this.props.shop ? this.props.shop.category_id : ''}>
                                    <option value={''}>Select Category</option>
                                    {categoryOptions}
                                </select>
                                {this.props.editShopErrors.category_id ? <div className={"invalid-feedback"}>
                                    {this.props.editShopErrors.category_id}
                                </div> : null}
                            </div>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.editShopErrors.manager_id})}>
                                <select
                                    className={classNames('form-control', {'is-invalid': this.props.editShopErrors.manager_id})}
                                    ref={(input) => this.managerEdit = input}
                                    defaultValue={this.props.shop ? this.props.shop.manager_id : ''}>
                                    <option value={''}>Select Manager</option>
                                    {managers}
                                </select>
                                {this.props.editShopErrors.manager_id ? <div className={"invalid-feedback"}>
                                    {this.props.editShopErrors.manager_id}
                                </div> : null}
                            </div>
                        </ModalBody>
                        <ModalFooter>
                            <Button color="primary">Save</Button>{' '}
                            <Button color="secondary" onClick={this.toggleEditModal}>Cancel</Button>
                        </ModalFooter>
                    </form>
                </Modal>


                <Modal isOpen={this.state.deleteModal} toggle={this.toggleDeleteModal}
                       className={'modal-danger ' + this.props.className}>
                    <ModalHeader toggle={this.toggleDeleteModal}>Delete Shop</ModalHeader>
                    <ModalBody>
                        Are you sure that you want delete this shop?
                    </ModalBody>
                    <ModalFooter>
                        <Button color="danger" onClick={this.delete}>Delete</Button>{' '}
                        <Button color="secondary" onClick={this.toggleDeleteModal}>Cancel</Button>
                    </ModalFooter>
                </Modal>
                <BootstrapTable data={shops} version='4' options={this.state.options} pagination
                                striped={true} hover={true}>
                    <TableHeaderColumn dataField="name" isKey={true} dataSort={true}
                                       filter={{type: 'TextFilter', delay: 100}}>
                        Name</TableHeaderColumn>
                    <TableHeaderColumn dataField="phone_number" dataSort={true} filter={{
                        type: 'TextFilter',
                        delay: 100
                    }}>
                        Phone Number</TableHeaderColumn>
                    <TableHeaderColumn dataField="category" filterFormatted={true}
                                       dataFormat={this.categoryNameFormatter} dataSort={true}
                                       filter={{
                                           type: 'TextFilter',
                                           delay: 100
                                       }}>
                        Category</TableHeaderColumn>
                    <TableHeaderColumn dataField="manager" filterFormatted={true}
                                       dataFormat={this.managerNameFormatter} dataSort={true}
                                       filter={{
                                           type: 'TextFilter',
                                           delay: 100
                                       }}>
                        Manager</TableHeaderColumn>
                    <TableHeaderColumn dataField='actions'
                                       dataFormat={::this.actionFormatter}>Action</TableHeaderColumn>
                </BootstrapTable>
            </div>
        )
    }
}

export default Shop;
