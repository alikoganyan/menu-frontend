import React, {Component} from 'react';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import '../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css';
import {Button, Modal, ModalHeader, ModalBody, ModalFooter} from "reactstrap";
import classNames from 'classnames';
import {List} from 'immutable';

let orders = [];

class Order extends Component {
    constructor(props) {
        super(props);
        this.state = {
            options: {
                sizePerPage: 10,
                hideSizePerPage: true
            },
            orderId: 0,
            companyId: 0,
            connectDriverModal: false,
            cancelModal: false
        };
        this.all = this.all.bind(this);
        this.connect = this.connect.bind(this);
        this.cancel = this.cancel.bind(this);
        this.toggleConnectDriverModal = this.toggleConnectDriverModal.bind(this);
        this.toggleCancelModal = this.toggleCancelModal.bind(this);
    }

    componentDidMount() {
        if (this.props.match.params.companyId) {
            this.setState({
                companyId: this.props.match.params.companyId
            });
            this.all(this.props.match.params.companyId);
        }else {
            this.all(0);
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.mustGetOrders) {
            orders = this.props.orders;
        }
        if (nextProps.orderCreated) {
            orders = [this.props.order, ...orders];
            this.toggleCreateModal();
            this.props.setDefaults();
        }
        if (nextProps.orderCancelled) {
            const index = orders.findIndex(item => item.id == this.state.orderId);
            let order = this.props.orderCancel;
            orders = List(orders).update(index, function () {
                return order;
            }).toArray();
            this.props.setDefaults();
        }
    }

    toggleCancelModal(id) {
        if (id) {
            this.setState({
                orderId: id
            });
        }
        this.setState({
            cancelModal: !this.state.cancelModal
        });
    }

    actionFormatter(cell, row) {
        return (<div>
            {row.type == 2 ? <Button color={'danger'} onClick={() => this.toggleCancelModal(row.id)}>
                Cancel
            </Button> : ''} {' '}
        </div>);
    }

    statusFormatter(cell, row) {
        let status = "";
        if (row.type == 1) {
            status = 'Pending';
        }
        if (row.type == 2) {
            status = 'Active';
        }
        if (row.type == 3) {
            status = 'Cancelled';
        }
        return status;
    }

    driverNameFormatter(cell, row) {
        return row.driver.name;
    }

    all(companyId) {
        this.props.all(companyId);
    }

    cancel(e) {
        e.preventDefault();
        this.props.cancel(this.state.orderId);
        this.toggleCancelModal();
    }

    render() {
        return (
            <div>
                <Modal isOpen={this.state.cancelModal} toggle={this.toggleCancelModal}
                       className={'modal-danger ' + this.props.className}>
                    <ModalHeader toggle={this.toggleCancelModal}>Delete Order</ModalHeader>
                    <ModalBody>
                        Are you sure that you want cancel this order?If you want this order all amount will refund.
                    </ModalBody>
                    <ModalFooter>
                        <Button color="danger" onClick={this.cancel}>Cancel</Button>{' '}
                        <Button color="secondary" onClick={this.toggleCancelModal}>Cancel</Button>
                    </ModalFooter>
                </Modal>
                <BootstrapTable data={orders} version='4' options={this.state.options} pagination
                                striped={true} hover={true}>
                    <TableHeaderColumn dataField="order_number" isKey={true}
                                       dataSort={true}
                                       filter={{type: 'TextFilter', delay: 100}}>
                        Order Number</TableHeaderColumn>
                    <TableHeaderColumn dataField="driver" dataFormat={this.driverNameFormatter}
                                       dataSort={true}
                                       filter={{type: 'TextFilter', delay: 100}}>
                        Driver</TableHeaderColumn>
                    <TableHeaderColumn dataField="total"
                                       dataSort={true}
                                       filter={{type: 'TextFilter', delay: 100}}>
                        Total</TableHeaderColumn>
                    <TableHeaderColumn dataField="address_name"
                                       dataSort={true}
                                       filter={{type: 'TextFilter', delay: 100}}>
                        Address Name</TableHeaderColumn>
                    <TableHeaderColumn dataField="type"
                                       dataSort={true}
                                       dataFormat={this.statusFormatter}
                                       filter={{type: 'TextFilter', delay: 100}}>
                        Status</TableHeaderColumn>
                    <TableHeaderColumn dataField='actions'
                                       dataFormat={::this.actionFormatter}>Action</TableHeaderColumn>
                </BootstrapTable>
            </div>
        )
    }
}

export default Order;
