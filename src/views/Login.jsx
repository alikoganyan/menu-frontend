import React, {Component} from "react";
import {
    Container,
    Row,
    Col,
    CardGroup,
    Card,
    CardBlock,
    CardHeader,
    Button,
    InputGroup,
    InputGroupAddon
} from "reactstrap";

class Login extends Component {
    constructor(props) {
        super(props);
        this.login = this.login.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.loginSuccess) {
            this.props.history.push("dashboard");
        }
    }

    login() {
        this.props.login(this.username.value, this.password.value);
    }

    render() {
        return (
            <div className="app flex-row align-items-center">
                <Container>
                    <Row className="justify-content-center">
                        <Col md="5">
                            <CardGroup className="mb-0">
                                <Card className="p-4">
                                    <CardBlock className="card-body">
                                        <h1>Login</h1>
                                        <p className="text-muted">Sign In to your account</p>
                                        {this.props.loginErrors.credentials ?
                                            <Card className="text-white text-center bg-danger">
                                                <CardHeader>
                                                    Wrong credentials
                                                </CardHeader>
                                            </Card> : null}
                                        <InputGroup className="mb-3">
                                            <InputGroupAddon><i className="icon-user"></i></InputGroupAddon>
                                            <input type="text" placeholder="Username" className="form-control"
                                                   ref={(input) => this.username = input}/>
                                        </InputGroup>
                                        <InputGroup className="mb-4">
                                            <InputGroupAddon><i className="icon-lock"></i></InputGroupAddon>
                                            <input type="password" placeholder="Password" className="form-control"
                                                   ref={(input) => this.password = input}/>
                                        </InputGroup>
                                        <Row>
                                            <Col xs="6">
                                                <Button color="primary" className="px-4"
                                                        onClick={this.login}>Login</Button>
                                            </Col>
                                        </Row>
                                    </CardBlock>
                                </Card>
                            </CardGroup>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}

export default Login;
