import React, {Component} from 'react';
import '../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css';
import {Button, Card, CardHeader} from "reactstrap";
import {Map} from 'immutable';

let defaultTranslations = [];
let translations = [];
let translationsObject = {};
let component;

class DefaultTranslation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            options: {
                sizePerPage: 10,
                hideSizePerPage: true
            },
            defaultTranslationId: 0,
            createModal: false,
            editModal: false,
            deleteModal: false
        };
        this.all = this.all.bind(this);
        this.edit = this.edit.bind(this);
    }

    componentDidMount() {
        this.setState({
            languageCode: this.props.match.params.languageCode
        });
        this.all(this.props.match.params.languageCode);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.mustGetDefaultTranslations) {
            defaultTranslations = this.props.defaultTranslations;
            translations = Map(defaultTranslations).toArray().map(function (translationObjects, key) {
                translationsObject[Object.keys(defaultTranslations)[key]] = translationObjects;
            });
            translations = Map(defaultTranslations).toSeq().sortBy((v, k) => k).toArray().map((translationObjects, key) =>
                <div
                    className={'form-group'} key={key}>
                    <input type="text" placeholder="Name" key={key + 1} onChange={this.onChange.bind(this, key)}
                           className={"form-control"} defaultValue={translationObjects}/>
                </div>);
        }
        if (nextProps.defaultTranslationEdited) {
            component = this;
            setTimeout(function () {
                component.props.setDefaults();
            }, 1000);
        }
    }

    all(languageCode) {
        this.props.all(languageCode);
    }

    onChange(key, e) {
        translationsObject[Object.keys(this.props.defaultTranslations)[key]] = e.target.value;
    }

    edit() {
        this.props.edit(this.state.languageCode, translationsObject);
        window.scrollTo(0, 0);
    }

    render() {
        return (
            <div>
                {this.props.defaultTranslationEdited ?
                    <Card className="text-white text-center bg-success">
                        <CardHeader>
                            Successfully Translated
                        </CardHeader>
                    </Card> : null}
                <form ref={(input) => {
                    this.form = input
                }} onSubmit={this.edit}>
                    {translations}
                    <Button color={'primary'} style={{float: 'right', marginBottom: '20px'}}
                            onClick={this.edit}>Translate</Button>
                </form>
            </div>
        )
    }
}

export default DefaultTranslation;
