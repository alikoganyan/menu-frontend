import React, {Component} from 'react';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import '../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css';
import {Button, Modal, ModalHeader, ModalBody, ModalFooter} from "reactstrap";
import classNames from 'classnames';
import {List} from 'immutable';
import {Link} from "react-router-dom";
import cookie from "react-cookies";

let languages = [];

class Language extends Component {
    constructor(props) {
        super(props);
        this.state = {
            options: {
                sizePerPage: 10,
                hideSizePerPage: true
            },
            languageId: 0,
            createModal: false,
            editModal: false,
            deleteModal: false
        };
        this.all = this.all.bind(this);
        this.get = this.get.bind(this);
        this.add = this.add.bind(this);
        this.edit = this.edit.bind(this);
        this.delete = this.delete.bind(this);
        this.toggleCreateModal = this.toggleCreateModal.bind(this);
        this.toggleEditModal = this.toggleEditModal.bind(this);
        this.toggleDeleteModal = this.toggleDeleteModal.bind(this);
        this.nameColumnFormatter = this.nameColumnFormatter.bind(this);
    }

    componentDidMount() {
        this.all();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.mustGetLanguages) {
            languages = this.props.languages;
        }
        if (nextProps.languageCreated) {
            languages = [this.props.language, ...languages];
            this.toggleCreateModal();
            this.props.setDefaults();
        }
        if (nextProps.languageEdited) {
            this.toggleEditModal();
            const index = languages.findIndex(item => item.id == this.state.languageId);
            let language = this.props.language;
            languages = List(languages).update(index, function () {
                return language;
            }).toArray();
            this.props.setDefaults();
        }
        if (nextProps.languageGet) {
            this.toggleEditModal();
            this.setState({
                languageId: this.props.language.id
            });
            this.props.setDefaults();
        }
    }

    toggleCreateModal() {
        this.setState({
            createModal: !this.state.createModal
        });
    }

    toggleEditModal() {
        this.setState({
            editModal: !this.state.editModal
        });
    }

    toggleDeleteModal(id) {
        if (id) {
            this.setState({
                languageId: id
            });
        }
        this.setState({
            deleteModal: !this.state.deleteModal
        });
    }

    actionFormatter(cell, row) {
        return (<div><Button color={'success'} onClick={() => this.get(row.id)}>
            Edit
        </Button>{' '}
            <Button color={'danger'} onClick={() => this.toggleDeleteModal(row.id)}>
                Delete
            </Button>{' '}
            <Link to={'/default-translations/' + row.language_code}><Button color={'success'}>
                <i className="fa fa-flag"></i><br/>
            </Button></Link>
        </div>);
    }

    all() {
        this.props.all(0); //TODO: must set user company id
    }

    get(id) {
        this.props.get(id);
        this.setState({languageId: id});
    }

    add(e) {
        e.preventDefault();
        this.props.add(this.name.value);
    }

    edit(e) {
        e.preventDefault();
        this.props.edit(this.state.languageId, this.nameEdit.value);
    }

    delete() {
        this.props.delete(this.state.languageId);
        this.toggleDeleteModal();
        const index = languages.findIndex(item => item.id == this.state.languageId);
        languages = List(languages).delete(index).toArray();
    }

    nameColumnFormatter(cell, row) {
        return (<Link to={'/default-translations/' + row.language_code}>{row.name}</Link>);
    }

    render() {
        return (
            <div>
                {cookie.load('user').role_id==2 || cookie.load('user').role_id==3 ? <Button color={'success'} onClick={this.toggleCreateModal} style={{'marginBottom':'20px'}}>Add Language</Button>
                    : ''}
                <Modal isOpen={this.state.createModal} toggle={this.toggleCreateModal} className={this.props.className}>
                    <form onSubmit={this.add}>
                        <ModalHeader toggle={this.toggleCreateModal}>Add Language</ModalHeader>
                        <ModalBody>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.createLanguageErrors.name})}>
                                <input type="text" placeholder="Name"
                                       className={classNames("form-control", {'is-invalid': this.props.createLanguageErrors.name})}
                                       ref={(input) => this.name = input}/>
                                {this.props.createLanguageErrors.name ? <div className={"invalid-feedback"}>
                                    {this.props.createLanguageErrors.name}
                                </div> : null}
                            </div>
                        </ModalBody>
                        <ModalFooter>
                            <Button color="primary">Add</Button>{' '}
                            <Button color="secondary" onClick={this.toggleCreateModal}>Cancel</Button>
                        </ModalFooter>
                    </form>
                </Modal>

                <Modal isOpen={this.state.editModal} toggle={this.toggleEditModal} className={this.props.className}>
                    <form onSubmit={this.edit}>
                        <ModalHeader toggle={this.toggleEditModal}>Edit Language</ModalHeader>
                        <ModalBody>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.editLanguageErrors.name})}>
                                <input type="text" placeholder="Name"
                                       className={classNames("form-control", {'is-invalid': this.props.editLanguageErrors.name})}
                                       ref={(input) => this.nameEdit = input}
                                       defaultValue={this.props.language ? this.props.language.name : ''}/>
                                {this.props.editLanguageErrors.name ? <div className={"invalid-feedback"}>
                                    {this.props.editLanguageErrors.name}
                                </div> : null}
                            </div>
                        </ModalBody>
                        <ModalFooter>
                            <Button color="primary">Save</Button>{' '}
                            <Button color="secondary" onClick={this.toggleEditModal}>Cancel</Button>
                        </ModalFooter>
                    </form>
                </Modal>


                <Modal isOpen={this.state.deleteModal} toggle={this.toggleDeleteModal}
                       className={'modal-danger ' + this.props.className}>
                    <ModalHeader toggle={this.toggleDeleteModal}>Delete Language</ModalHeader>
                    <ModalBody>
                        Are you sure that you want delete this language?
                    </ModalBody>
                    <ModalFooter>
                        <Button color="danger" onClick={this.delete}>Delete</Button>{' '}
                        <Button color="secondary" onClick={this.toggleDeleteModal}>Cancel</Button>
                    </ModalFooter>
                </Modal>
                <BootstrapTable data={languages} version='4' options={this.state.options} pagination
                                striped={true} hover={true}>
                    <TableHeaderColumn dataField="name" isKey={true}
                                       dataSort={true}
                                       filter={{type: 'TextFilter', delay: 100}}>
                        Name</TableHeaderColumn>
                    <TableHeaderColumn dataField='actions'
                                       dataFormat={::this.actionFormatter}>Action</TableHeaderColumn>
                </BootstrapTable>
            </div>
        )
    }
}

export default Language;
