import React, {Component} from 'react';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import '../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css';
import {Button, Modal, ModalHeader, ModalBody, ModalFooter, Col} from "reactstrap";
import classNames from 'classnames';
import {List} from 'immutable';
import {Link} from "react-router-dom";

let companies = [];

class Company extends Component {
    constructor(props) {
        super(props);
        this.state = {
            options: {
                sizePerPage: 10,
                hideSizePerPage: true
            },
            companyId: 0,
            createModal: false,
            editModal: false,
            deleteModal: false,
        };
        this.all = this.all.bind(this);
        this.add = this.add.bind(this);
        this.get = this.get.bind(this);
        this.edit = this.edit.bind(this);
        this.delete = this.delete.bind(this);
        this.toggleCreateModal = this.toggleCreateModal.bind(this);
        this.toggleEditModal = this.toggleEditModal.bind(this);
        this.toggleDeleteModal = this.toggleDeleteModal.bind(this);
    }

    componentDidMount() {
        this.all();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.mustGetCompanies) {
            companies = this.props.companies;
        }
        if (nextProps.companyCreated) {
            companies = [this.props.company, ...companies];
            this.toggleCreateModal();
            this.props.setDefaults();
        }
        if (nextProps.companyEdited) {
            this.toggleEditModal();
            const index = companies.findIndex(item => item.id == this.state.companyId);
            let company = this.props.company;
            companies = List(companies).update(index, function () {
                return company;
            }).toArray();
            this.props.setDefaults();
        }
        if (nextProps.companyGet) {
            this.toggleEditModal();
            this.setState({
                companyId: this.props.company.id
            });
            this.props.setDefaults();
        }
    }

    toggleCreateModal() {
        this.setState({
            createModal: !this.state.createModal
        });
    }

    toggleEditModal() {
        this.setState({
            editModal: !this.state.editModal
        });
    }

    toggleDeleteModal(id) {
        if (id) {
            this.setState({
                companyId: id
            });
        }
        this.setState({
            deleteModal: !this.state.deleteModal
        });
    }

    actionFormatter(cell, row) {
        return (<div><Button color={'success'} onClick={() => this.get(row.id)}>
            Edit
        </Button>{' '}
            <Button color={'danger'} onClick={() => this.toggleDeleteModal(row.id)}>
                Delete
            </Button>{' '}
            <Link to={'/company-users/' + row.id}><Button color={'success'}>
                <i className="fa fa-users"></i><br/>
            </Button></Link>{' '}
            <Link to={'/orders/' + row.id}><Button color={'success'}>
                <i className="fa fa-dollar"></i><br/>
            </Button></Link>
        </div>);
    }

    all() {
        this.props.all();
    }

    get(id) {
        this.props.get(id);
        this.setState({companyId: id});
    }

    add(e) {
        e.preventDefault();
        this.props.add(this.name.value, this.domain.value, this.email.value, this.username.value, this.password.value, this.confirmPassword.value);
    }

    edit(e) {
        e.preventDefault();
        this.props.edit(this.state.companyId, this.nameEdit.value, this.domainEdit.value, this.emailEdit.value, this.usernameEdit.value, this.passwordEdit.value, this.confirmPasswordEdit.value);
    }

    delete() {
        this.props.delete(this.state.companyId);
        this.toggleDeleteModal();
        const index = companies.findIndex(item => item.id == this.state.companyId);
        companies = List(companies).delete(index).toArray();
    }

    render() {
        return (
            <div>
                <Button color={'success'} onClick={this.toggleCreateModal}>Add Company</Button>
                <br/>
                <br/>
                <Modal isOpen={this.state.createModal} toggle={this.toggleCreateModal} className={this.props.className}>
                    <form onSubmit={this.add}>
                        <ModalHeader toggle={this.toggleCreateModal}>Add Company</ModalHeader>
                        <ModalBody>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.createCompanyErrors.name})}>
                                <input type="text" placeholder="Name"
                                       className={classNames("form-control", {'is-invalid': this.props.createCompanyErrors.name})}
                                       ref={(input) => this.name = input}/>
                                {this.props.createCompanyErrors.name ? <div className={"invalid-feedback"}>
                                    {this.props.createCompanyErrors.name}
                                </div> : null}
                            </div>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.createCompanyErrors.domain})}>
                                <input type="text" placeholder="Domain"
                                       className={classNames("form-control", {'is-invalid': this.props.createCompanyErrors.domain})}
                                       ref={(input) => this.domain = input}/>
                                {this.props.createCompanyErrors.domain ? <div className={"invalid-feedback"}>
                                    {this.props.createCompanyErrors.domain}
                                </div> : null}
                            </div>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.createCompanyErrors.email})}>
                                <input type="text" placeholder="E-mail"
                                       className={classNames("form-control", {'is-invalid': this.props.createCompanyErrors.email})}
                                       ref={(input) => this.email = input}/>
                                {this.props.createCompanyErrors.email ? <div className={"invalid-feedback"}>
                                    {this.props.createCompanyErrors.email}
                                </div> : null}
                            </div>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.createCompanyErrors.username})}>
                                <input type="text" placeholder="Username"
                                       className={classNames("form-control", {'is-invalid': this.props.createCompanyErrors.username})}
                                       ref={(input) => this.username = input}/>
                                {this.props.createCompanyErrors.username ? <div className={"invalid-feedback"}>
                                    {this.props.createCompanyErrors.username}
                                </div> : null}
                            </div>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.createCompanyErrors.password})}>
                                <input type="password" placeholder="Password"
                                       className={classNames("form-control", {'is-invalid': this.props.createCompanyErrors.password})}
                                       ref={(input) => this.password = input}/>
                                {this.props.createCompanyErrors.password ? <div className={"invalid-feedback"}>
                                    {this.props.createCompanyErrors.password}
                                </div> : null}
                            </div>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.createCompanyErrors.confirm_password})}>
                                <input type="password" placeholder="Confirm Password"
                                       className={classNames("form-control", {'is-invalid': this.props.createCompanyErrors.confirm_password})}
                                       ref={(input) => this.confirmPassword = input}/>
                                {this.props.createCompanyErrors.confirm_password ? <div className={"invalid-feedback"}>
                                    {this.props.createCompanyErrors.confirm_password}
                                </div> : null}
                            </div>
                        </ModalBody>
                        <ModalFooter>
                            <Button color="primary">Add</Button>{' '}
                            <Button color="secondary" onClick={this.toggleCreateModal}>Cancel</Button>
                        </ModalFooter>
                    </form>
                </Modal>

                <Modal isOpen={this.state.editModal} toggle={this.toggleEditModal} className={this.props.className}>
                    <form onSubmit={this.edit}>
                        <ModalHeader toggle={this.toggleEditModal}>Edit Company</ModalHeader>
                        <ModalBody>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.editCompanyErrors.name})}>
                                <input type="text" placeholder="Name"
                                       className={classNames("form-control", {'is-invalid': this.props.editCompanyErrors.name})}
                                       ref={(input) => this.nameEdit = input}
                                       defaultValue={this.props.company ? this.props.company.name : ''}/>
                                {this.props.editCompanyErrors.name ? <div className={"invalid-feedback"}>
                                    {this.props.editCompanyErrors.name}
                                </div> : null}
                            </div>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.editCompanyErrors.domain})}>
                                <input type="text" placeholder="Domain"
                                       className={classNames("form-control", {'is-invalid': this.props.editCompanyErrors.domain})}
                                       ref={(input) => this.domainEdit = input}
                                       defaultValue={this.props.company ? this.props.company.domain : ''}/>
                                {this.props.editCompanyErrors.domain ? <div className={"invalid-feedback"}>
                                    {this.props.editCompanyErrors.domain}
                                </div> : null}
                            </div>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.editCompanyErrors.email})}>
                                <input type="text" placeholder="E-mail"
                                       className={classNames("form-control", {'is-invalid': this.props.editCompanyErrors.email})}
                                       ref={(input) => this.emailEdit = input}
                                       defaultValue={this.props.company ? this.props.company.email : ''}/>
                                {this.props.editCompanyErrors.email ? <div className={"invalid-feedback"}>
                                    {this.props.editCompanyErrors.email}
                                </div> : null}
                            </div>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.editCompanyErrors.username})}>
                                <input type="text" placeholder="Username"
                                       className={classNames("form-control", {'is-invalid': this.props.editCompanyErrors.username})}
                                       ref={(input) => this.usernameEdit = input}
                                       defaultValue={this.props.company ? this.props.company.admin.username : ''}/>
                                {this.props.editCompanyErrors.username ? <div className={"invalid-feedback"}>
                                    {this.props.editCompanyErrors.username}
                                </div> : null}
                            </div>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.editCompanyErrors.password})}>
                                <input type="password" placeholder="Password"
                                       className={classNames("form-control", {'is-invalid': this.props.editCompanyErrors.password})}
                                       ref={(input) => this.passwordEdit = input}/>
                                {this.props.editCompanyErrors.password ? <div className={"invalid-feedback"}>
                                    {this.props.editCompanyErrors.password}
                                </div> : null}
                            </div>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.editCompanyErrors.confirmPassword})}>
                                <input type="password" placeholder="Confirm Password"
                                       className={classNames("form-control", {'is-invalid': this.props.editCompanyErrors.confirmPassword})}
                                       ref={(input) => this.confirmPasswordEdit = input}/>
                                {this.props.editCompanyErrors.confirmPassword ? <div className={"invalid-feedback"}>
                                    {this.props.editCompanyErrors.confirmPassword}
                                </div> : null}
                            </div>
                        </ModalBody>
                        <ModalFooter>
                            <Button color="primary">Save</Button>{' '}
                            <Button color="secondary" onClick={this.toggleEditModal}>Cancel</Button>
                        </ModalFooter>
                    </form>
                </Modal>


                <Modal isOpen={this.state.deleteModal} toggle={this.toggleDeleteModal}
                       className={'modal-danger ' + this.props.className}>
                    <ModalHeader toggle={this.toggleDeleteModal}>Delete Company</ModalHeader>
                    <ModalBody>
                        Are you sure that you want delete this company?
                    </ModalBody>
                    <ModalFooter>
                        <Button color="danger" onClick={this.delete}>Delete</Button>{' '}
                        <Button color="secondary" onClick={this.toggleDeleteModal}>Cancel</Button>
                    </ModalFooter>
                </Modal>
                <BootstrapTable data={companies} version='4' options={this.state.options} pagination
                                striped={true} hover={true}>
                    <TableHeaderColumn dataField="name" isKey={true} dataSort={true}
                                       filter={{type: 'TextFilter', delay: 100}}>
                        Name</TableHeaderColumn>
                    <TableHeaderColumn dataField="domain" dataSort={true} filter={{
                        type: 'TextFilter',
                        delay: 100
                    }}>
                        Domain</TableHeaderColumn>
                    <TableHeaderColumn dataField="email" dataSort={true} filter={{
                        type: 'TextFilter',
                        delay: 100
                    }}>
                        E-mail</TableHeaderColumn>
                    <TableHeaderColumn dataField='actions'
                                       dataFormat={::this.actionFormatter}>Action</TableHeaderColumn>
                </BootstrapTable>
            </div>
        )
    }
}

export default Company;
