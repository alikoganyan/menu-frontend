import React, {Component} from 'react';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import '../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css';
import {Button, Modal, ModalHeader, ModalBody, ModalFooter} from "reactstrap";
import classNames from 'classnames';
import {List} from 'immutable';
import PlacesAutocomplete, {geocodeByAddress, getLatLng} from 'react-places-autocomplete'
import {Link} from "react-router-dom";
import cookie from "react-cookies";

let restaurants = [];
let restaurantTypes = [];
let managers = [];

class Restaurant extends Component {
    constructor(props) {
        super(props);
        this.state = {
            options: {
                sizePerPage: 10,
                hideSizePerPage: true
            },
            restaurantId: 0,
            createModal: false,
            editModal: false,
            deleteModal: false,
            createRestaurantAddressName: '',
            editRestaurantAddressName: ''
        };
        this.changeCreateRestaurantAddressInput = (createRestaurantAddressName) => this.setState({createRestaurantAddressName});
        this.changeEditRestaurantAddressInput = (editRestaurantAddressName) => this.setState({editRestaurantAddressName});
        this.all = this.all.bind(this);
        this.get = this.get.bind(this);
        this.getRestaurantTypes = this.getRestaurantTypes.bind(this);
        this.add = this.add.bind(this);
        this.edit = this.edit.bind(this);
        this.delete = this.delete.bind(this);
        this.toggleCreateModal = this.toggleCreateModal.bind(this);
        this.toggleEditModal = this.toggleEditModal.bind(this);
        this.toggleDeleteModal = this.toggleDeleteModal.bind(this);
    }

    componentDidMount() {
        this.all();
        this.getRestaurantTypes();
        if (cookie.load('user').role_id == 2) {
            this.props.getManagers(cookie.load('user').company_id);
        } else {
            this.props.getManagers();
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.mustGetRestaurants) {
            restaurants = this.props.restaurants;
        }
        if (nextProps.restaurantTypes) {
            restaurantTypes = nextProps.restaurantTypes;
        }
        if (nextProps.restaurantCreated) {
            restaurants = [this.props.restaurant, ...restaurants];
            this.toggleCreateModal();
            this.setState({
                createRestaurantAddressName: ""
            });
            this.props.setDefaults();
        }
        if (nextProps.restaurantEdited) {
            this.toggleEditModal();
            const index = restaurants.findIndex(item => item.id == this.state.restaurantId);
            let restaurant = this.props.restaurant;
            restaurants = List(restaurants).update(index, function () {
                return restaurant;
            }).toArray();
            this.props.setDefaults();
        }
        if (nextProps.managers) {
            managers = nextProps.managers.map((manager, key) =>
                <option key={key} value={manager.id}>{manager.name}</option>
            )
        }
        if (nextProps.restaurantGet) {
            this.toggleEditModal();
            this.setState({
                restaurantId: this.props.restaurant.id,
                editRestaurantAddressName: this.props.restaurant.address_name
            });
            this.props.setDefaults();
        }
    }

    toggleCreateModal() {
        this.setState({
            createModal: !this.state.createModal
        });
    }

    toggleEditModal() {
        this.setState({
            editModal: !this.state.editModal
        });
    }

    toggleDeleteModal(id) {
        if (id) {
            this.setState({
                restaurantId: id
            });
        }
        this.setState({
            deleteModal: !this.state.deleteModal
        });
    }

    actionFormatter(cell, row) {
        return (<div><Button color={'success'} onClick={() => this.get(row.id)}>
            Edit
        </Button>{' '}
            <Button color={'danger'} onClick={() => this.toggleDeleteModal(row.id)}>
                Delete
            </Button>{' '}
            <Link to={'/product-sections/' + row.id}><Button color={'success'}>
                <i className="fa fa-dropbox"></i><br/>
            </Button></Link>
        </div>);
    }

    typeNameFormatter(cell, row) {
        return row.type.default_name;
    }

    managerNameFormatter(cell, row) {
        let managerName = "";
        if (row.manager) {
            managerName = row.manager.name;
        }
        return managerName;
    }

    all() {
        this.props.all();
    }

    get(id) {
        this.props.get(id);
        this.setState({restaurantId: id});
    }

    getRestaurantTypes() {
        this.props.getRestaurantTypes();
    }

    add(e) {
        e.preventDefault();
        let component = this;
        geocodeByAddress(this.state.createRestaurantAddressName)
            .then(results => getLatLng(results[0]))
            .then(function (latLng) {
                component.props.add(component.name.value, component.phoneNumber.value, component.state.createRestaurantAddressName, latLng.lat, latLng.lng, component.type.value, component.manager.value)
            })
            .catch(function (error) {
                component.props.add(component.name.value, component.phoneNumber.value, component.state.createRestaurantAddressName, '', '', component.type.value, component.manager.value);
            });
    }

    edit(e) {
        e.preventDefault();
        let component = this;
        geocodeByAddress(this.state.editRestaurantAddressName)
            .then(results => getLatLng(results[0]))
            .then(function (latLng) {
                component.props.edit(component.state.restaurantId, component.nameEdit.value, component.phoneNumberEdit.value, component.state.editRestaurantAddressName, latLng.lat, latLng.lng, component.typeEdit.value, component.managerEdit.value);
            })
            .catch(function (error) {
                component.props.edit(component.state.restaurantId, component.nameEdit.value, component.phoneNumberEdit.value, component.state.editRestaurantAddressName, '', '', component.typeEdit.value, component.managerEdit.value);
            });
    }

    delete() {
        this.props.delete(this.state.restaurantId);
        this.toggleDeleteModal();
        const index = restaurants.findIndex(item => item.id == this.state.restaurantId);
        restaurants = List(restaurants).delete(index).toArray();
    }

    render() {
        const createRestaurantAddressNameInputProps = {
            value: this.state.createRestaurantAddressName,
            placeholder: 'Search Address',
            onChange: this.changeCreateRestaurantAddressInput,
        };
        const editRestaurantAddressNameInputProps = {
            value: this.state.editRestaurantAddressName,
            placeholder: 'Search Address',
            onChange: this.changeEditRestaurantAddressInput,
        };
        const restaurantTypeOptions = restaurantTypes.map((restaurantType, key) =>
            <option value={restaurantType.id} key={key}>{restaurantType.default_name}</option>
        );
        return (
            <div>
                {cookie.load('user').role_id == 2 || cookie.load('user').role_id == 3 ?
                    <Button color={'success'} onClick={this.toggleCreateModal} style={{'marginBottom': '20px'}}>Add
                        Restaurant</Button>
                    : ''}
                <br/>
                <br/>
                <Modal isOpen={this.state.createModal} toggle={this.toggleCreateModal} className={this.props.className}>
                    <form onSubmit={this.add}>
                        <ModalHeader toggle={this.toggleCreateModal}>Add Restaurant</ModalHeader>
                        <ModalBody>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.createRestaurantErrors.name})}>
                                <input type="text" placeholder="Name"
                                       className={classNames("form-control", {'is-invalid': this.props.createRestaurantErrors.name})}
                                       ref={(input) => this.name = input}/>
                                {this.props.createRestaurantErrors.name ? <div className={"invalid-feedback"}>
                                    {this.props.createRestaurantErrors.name}
                                </div> : null}
                            </div>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.createRestaurantErrors.phone_number})}>
                                <input type="text" placeholder="Phone Number"
                                       className={classNames("form-control", {'is-invalid': this.props.createRestaurantErrors.phone_number})}
                                       ref={(input) => this.phoneNumber = input}/>
                                {this.props.createRestaurantErrors.phone_number ? <div className={"invalid-feedback"}>
                                    {this.props.createRestaurantErrors.phone_number}
                                </div> : null}
                            </div>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.createRestaurantErrors.address_name})}>
                                <PlacesAutocomplete inputProps={createRestaurantAddressNameInputProps}
                                                    classNames={{input: classNames("form-control", {'is-invalid': this.props.createRestaurantErrors.address_name || this.props.createRestaurantErrors.address_lat || this.props.createRestaurantErrors.address_lng})}}/>
                                {this.props.createRestaurantErrors.address_name || this.props.createRestaurantErrors.address_lat || this.props.createRestaurantErrors.address_lng ?
                                    <div className={"invalid-feedback"} style={{display: 'block'}}>
                                        {this.props.createRestaurantErrors.address_name}
                                    </div> : null}
                            </div>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.createRestaurantErrors.type_id})}>
                                <select
                                    className={classNames('form-control', {'is-invalid': this.props.createRestaurantErrors.type_id})}
                                    ref={(input) => this.type = input}>
                                    <option value={''}>Select Type</option>
                                    {restaurantTypeOptions}
                                </select>
                                {this.props.createRestaurantErrors.type_id ? <div className={"invalid-feedback"}>
                                    {this.props.createRestaurantErrors.type_id}
                                </div> : null}
                            </div>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.createRestaurantErrors.manager_id})}>
                                <select
                                    className={classNames('form-control', {'is-invalid': this.props.createRestaurantErrors.manager_id})}
                                    ref={(input) => this.manager = input}>
                                    <option value={''}>Select Manager</option>
                                    {managers}
                                </select>
                                {this.props.createRestaurantErrors.manager_id ? <div className={"invalid-feedback"}>
                                    {this.props.createRestaurantErrors.manager_id}
                                </div> : null}
                            </div>
                        </ModalBody>
                        <ModalFooter>
                            <Button color="primary">Add</Button>{' '}
                            <Button color="secondary" onClick={this.toggleCreateModal}>Cancel</Button>
                        </ModalFooter>
                    </form>
                </Modal>

                <Modal isOpen={this.state.editModal} toggle={this.toggleEditModal} className={this.props.className}>
                    <form onSubmit={this.edit}>
                        <ModalHeader toggle={this.toggleEditModal}>Edit Restaurant</ModalHeader>
                        <ModalBody>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.editRestaurantErrors.name})}>
                                <input type="text" placeholder="Name"
                                       className={classNames("form-control", {'is-invalid': this.props.editRestaurantErrors.name})}
                                       ref={(input) => this.nameEdit = input}
                                       defaultValue={this.props.restaurant ? this.props.restaurant.name : ''}/>
                                {this.props.editRestaurantErrors.name ? <div className={"invalid-feedback"}>
                                    {this.props.editRestaurantErrors.name}
                                </div> : null}
                            </div>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.editRestaurantErrors.phone_number})}>
                                <input type="text" placeholder="Phone Number"
                                       className={classNames("form-control", {'is-invalid': this.props.editRestaurantErrors.phone_number})}
                                       ref={(input) => this.phoneNumberEdit = input}
                                       defaultValue={this.props.restaurant ? this.props.restaurant.phone_number : ''}/>
                                {this.props.editRestaurantErrors.phone_number ? <div className={"invalid-feedback"}>
                                    {this.props.editRestaurantErrors.phone_number}
                                </div> : null}
                            </div>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.editRestaurantErrors.address_name})}>
                                <PlacesAutocomplete inputProps={editRestaurantAddressNameInputProps}
                                                    classNames={{input: classNames("form-control", {'is-invalid': this.props.editRestaurantErrors.address_name || this.props.editRestaurantErrors.address_lat || this.props.editRestaurantErrors.address_lng})}}/>
                                {this.props.editRestaurantErrors.address_name || this.props.editRestaurantErrors.address_lat || this.props.editRestaurantErrors.address_lng ?
                                    <div className={"invalid-feedback"} style={{display: 'block'}}>
                                        {this.props.editRestaurantErrors.address_name}
                                    </div> : null}
                            </div>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.editRestaurantErrors.type_id})}>
                                <select
                                    className={classNames('form-control', {'is-invalid': this.props.editRestaurantErrors.type_id})}
                                    ref={(input) => this.typeEdit = input}
                                    defaultValue={this.props.restaurant ? this.props.restaurant.type_id : ''}>
                                    <option value={''}>Select Type</option>
                                    {restaurantTypeOptions}
                                </select>
                                {this.props.editRestaurantErrors.type_id ? <div className={"invalid-feedback"}>
                                    {this.props.editRestaurantErrors.type_id}
                                </div> : null}
                            </div>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.editRestaurantErrors.manager_id})}>
                                <select
                                    className={classNames('form-control', {'is-invalid': this.props.editRestaurantErrors.manager_id})}
                                    ref={(input) => this.managerEdit = input}
                                    defaultValue={this.props.restaurant ? this.props.restaurant.manager_id : ''}>
                                    <option value={''}>Select Manager</option>
                                    {managers}
                                </select>
                                {this.props.editRestaurantErrors.manager_id ? <div className={"invalid-feedback"}>
                                    {this.props.editRestaurantErrors.manager_id}
                                </div> : null}
                            </div>
                        </ModalBody>
                        <ModalFooter>
                            <Button color="primary">Save</Button>{' '}
                            <Button color="secondary" onClick={this.toggleEditModal}>Cancel</Button>
                        </ModalFooter>
                    </form>
                </Modal>


                <Modal isOpen={this.state.deleteModal} toggle={this.toggleDeleteModal}
                       className={'modal-danger ' + this.props.className}>
                    <ModalHeader toggle={this.toggleDeleteModal}>Delete Restaurant</ModalHeader>
                    <ModalBody>
                        Are you sure that you want delete this restaurant?
                    </ModalBody>
                    <ModalFooter>
                        <Button color="danger" onClick={this.delete}>Delete</Button>{' '}
                        <Button color="secondary" onClick={this.toggleDeleteModal}>Cancel</Button>
                    </ModalFooter>
                </Modal>
                <BootstrapTable data={restaurants} version='4' options={this.state.options} pagination
                                striped={true} hover={true}>
                    <TableHeaderColumn dataField="name" isKey={true} dataSort={true}
                                       filter={{type: 'TextFilter', delay: 100}}>
                        Name</TableHeaderColumn>
                    <TableHeaderColumn dataField="phone_number" dataSort={true} filter={{
                        type: 'TextFilter',
                        delay: 100
                    }}>
                        Phone Number</TableHeaderColumn>
                    <TableHeaderColumn dataField="type" filterFormatted={true} dataFormat={this.typeNameFormatter}
                                       dataSort={true}
                                       filter={{
                                           type: 'TextFilter',
                                           delay: 100
                                       }}>
                        Restaurant Type</TableHeaderColumn>
                    <TableHeaderColumn dataField="type" filterFormatted={true} dataFormat={this.managerNameFormatter}
                                       dataSort={true}
                                       filter={{
                                           type: 'TextFilter',
                                           delay: 100
                                       }}>
                        Manager</TableHeaderColumn>
                    <TableHeaderColumn dataField='actions'
                                       dataFormat={::this.actionFormatter}>Action</TableHeaderColumn>
                </BootstrapTable>
            </div>
        )
    }
}

export default Restaurant;
