import React, {Component} from 'react';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import '../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css';
import {Button, Modal, ModalHeader, ModalBody, ModalFooter} from "reactstrap";
import classNames from 'classnames';
import {List, Map} from 'immutable';
import cookie from "react-cookies";

let restaurantTypes = [];
let companyLanguagesForCreate = [];
let companyLanguagesForEdit = [];
let languages = [];
let restaurantTypeNamesForCreate = {};
let restaurantTypeNamesForEdit = {};

class RestaurantType extends Component {
    constructor(props) {
        super(props);
        this.state = {
            options: {
                sizePerPage: 10,
                hideSizePerPage: true
            },
            restaurantTypeId: 0,
            restaurantType: [],
            createModal: false,
            editModal: false,
            deleteModal: false,
        };
        this.all = this.all.bind(this);
        this.getLanguages = this.getLanguages.bind(this);
        this.add = this.add.bind(this);
        this.get = this.get.bind(this);
        this.edit = this.edit.bind(this);
        this.delete = this.delete.bind(this);
        this.toggleCreateModal = this.toggleCreateModal.bind(this);
        this.toggleEditModal = this.toggleEditModal.bind(this);
        this.toggleDeleteModal = this.toggleDeleteModal.bind(this);
    }

    componentDidMount() {
        this.all();
        this.getLanguages();
    }

    componentDidUpdate() {
        if (this.props.languages) {
            languages = this.props.languages;
        }
    }

    componentWillReceiveProps(nextProps) {
        console.log(nextProps);
        if (nextProps.mustGetRestaurantTypes) {
            restaurantTypes = this.props.restaurantTypes;
        }
        if (nextProps.restaurantTypeCreated) {
            restaurantTypes = [this.props.restaurantType, ...restaurantTypes];
            this.toggleCreateModal();
            this.props.setDefaults();
            restaurantTypeNamesForCreate = {};
        }
        if (nextProps.restaurantTypeEdited) {
            this.toggleEditModal();
            const index = restaurantTypes.findIndex(item => item.id == this.state.restaurantTypeId);
            let restaurantType = this.props.restaurantType;
            restaurantTypes = List(restaurantTypes).update(index, function () {
                return restaurantType;
            }).toArray();
            this.props.setDefaults();
            restaurantTypeNamesForEdit = {};
        }
        if (nextProps.restaurantTypeGet) {
            languages.map(function (language, key) {
                if (nextProps.restaurantType.company_id == language.company_id) {
                    restaurantTypeNamesForEdit[nextProps.languages[key].language_code] = nextProps.restaurantType.translations[key] ? nextProps.restaurantType.translations[key].name : '';
                }
            });
            this.toggleEditModal();
            this.setState({
                restaurantTypeId: this.props.restaurantType.id,
                restaurantType: nextProps.restaurantType
            });
            this.props.setDefaults();
        }
    }

    toggleCreateModal() {
        this.setState({
            createModal: !this.state.createModal
        });
    }

    toggleEditModal() {
        this.setState({
            editModal: !this.state.editModal
        });
    }

    toggleDeleteModal(id) {
        if (id) {
            this.setState({
                restaurantTypeId: id
            });
        }
        this.setState({
            deleteModal: !this.state.deleteModal
        });
    }

    actionFormatter(cell, row) {
        return (<div><Button color={'success'} onClick={() => this.get(row.id)}>
            Edit
        </Button>{' '}
            <Button color={'danger'} onClick={() => this.toggleDeleteModal(row.id)}>
                Delete
            </Button></div>);
    }

    onChangeForCreate(key, e) {
        restaurantTypeNamesForCreate[languages[key].language_code] = e.target.value;
    }

    onChangeForEdit(key, e) {
        console.log(e.target.value);
        restaurantTypeNamesForEdit[languages[key].language_code] = e.target.value;
    }

    all() {
        this.props.all();
    }

    getLanguages() {
        this.props.getLanguages(0);
    }

    get(id) {
        this.props.get(id);
        this.setState({restaurantTypeId: id});
    }

    add(e) {
        e.preventDefault();
        this.props.add(restaurantTypeNamesForCreate);
    }

    edit(e) {
        e.preventDefault();
        this.props.edit(this.state.restaurantTypeId, restaurantTypeNamesForEdit);
    }

    delete() {
        this.props.delete(this.state.restaurantTypeId);
        this.toggleDeleteModal();
        const index = restaurantTypes.findIndex(item => item.id == this.state.restaurantTypeId);
        restaurantTypes = List(restaurantTypes).delete(index).toArray();
    }

    render() {
        companyLanguagesForCreate = languages.map((language, key) =>
            <div
                className={classNames('form-group', {'has-warning': this.props.createRestaurantTypeErrors && this.props.createRestaurantTypeErrors[language.language_code]})}
                key={key}>
                <input type="text" placeholder={"Name (for:" + language.name + ")"} key={key + 1}
                       onChange={this.onChangeForCreate.bind(this, key)}
                       className={classNames("form-control", {'is-invalid': this.props.createRestaurantTypeErrors && this.props.createRestaurantTypeErrors[language.language_code]})}/>
                {this.props.createRestaurantTypeErrors && this.props.createRestaurantTypeErrors[language.language_code] ?
                    <div className={"invalid-feedback"}>
                        {this.props.createRestaurantTypeErrors ? this.props.createRestaurantTypeErrors[language.language_code] : ''}
                    </div> : null}
            </div>);

        companyLanguagesForEdit = languages.map((language, key) =>
            this.state.restaurantType.company_id == language.company_id ?
                <div
                    className={classNames('form-group', {'has-warning': this.props.editRestaurantTypeErrors && this.props.editRestaurantTypeErrors[language.language_code]})}
                    key={key}>
                    <input type="text" placeholder={"Name (for:" + language.name + ")"} key={key + 1}
                           onChange={this.onChangeForEdit.bind(this, key)}
                           defaultValue={this.state.restaurantType.translations && this.state.restaurantType.translations[key] ? this.state.restaurantType.translations[key].name : ''}
                           className={classNames("form-control", {'is-invalid': this.props.editRestaurantTypeErrors && this.props.editRestaurantTypeErrors[language.language_code]})}/>
                    {this.props.editRestaurantTypeErrors && this.props.editRestaurantTypeErrors[language.language_code] ?
                        <div className={"invalid-feedback"}>
                            {this.props.editRestaurantTypeErrors ? this.props.editRestaurantTypeErrors[language.language_code] : ''}
                        </div> : null}
                </div> : '');
        return (
            <div>
                {cookie.load('user').role_id == 2 || cookie.load('user').role_id == 3 ?
                    <Button color={'success'} onClick={this.toggleCreateModal} style={{'marginBottom': '20px'}}>Add
                        Restaurant Type</Button>
                    : ''}
                <br/>
                <br/>
                <Modal isOpen={this.state.createModal} toggle={this.toggleCreateModal} className={this.props.className}>
                    <form onSubmit={this.add}>
                        <ModalHeader toggle={this.toggleCreateModal}>Add Restaurant Type</ModalHeader>
                        <ModalBody>
                            {companyLanguagesForCreate}
                        </ModalBody>
                        <ModalFooter>
                            <Button color="primary">Add</Button>{' '}
                            <Button color="secondary" onClick={this.toggleCreateModal}>Cancel</Button>
                        </ModalFooter>
                    </form>
                </Modal>

                <Modal isOpen={this.state.editModal} toggle={this.toggleEditModal} className={this.props.className}>
                    <form onSubmit={this.edit}>
                        <ModalHeader toggle={this.toggleEditModal}>Edit Restaurant Type</ModalHeader>
                        <ModalBody>
                            {companyLanguagesForEdit}
                        </ModalBody>
                        <ModalFooter>
                            <Button color="primary">Save</Button>{' '}
                            <Button color="secondary" onClick={this.toggleEditModal}>Cancel</Button>
                        </ModalFooter>
                    </form>
                </Modal>


                <Modal isOpen={this.state.deleteModal} toggle={this.toggleDeleteModal}
                       className={'modal-danger ' + this.props.className}>
                    <ModalHeader toggle={this.toggleDeleteModal}>Delete Restaurant Type</ModalHeader>
                    <ModalBody>
                        Are you sure that you want delete this restaurant type?
                    </ModalBody>
                    <ModalFooter>
                        <Button color="danger" onClick={this.delete}>Delete</Button>{' '}
                        <Button color="secondary" onClick={this.toggleDeleteModal}>Cancel</Button>
                    </ModalFooter>
                </Modal>
                <BootstrapTable data={restaurantTypes} version='4' options={this.state.options} pagination
                                striped={true} hover={true}>
                    <TableHeaderColumn dataField="default_name" isKey={true} dataSort={true}
                                       filter={{type: 'TextFilter', delay: 100}}>
                        Name</TableHeaderColumn>
                    <TableHeaderColumn dataField='actions'
                                       dataFormat={::this.actionFormatter}>Action</TableHeaderColumn>
                </BootstrapTable>
            </div>
        )
    }
}

export default RestaurantType;
