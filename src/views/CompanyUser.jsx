import React, {Component} from 'react';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import '../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css';
import {Button, Modal, ModalHeader, ModalBody, ModalFooter} from "reactstrap";
import classNames from 'classnames';
import {List} from "immutable";

let companyUsers=[];
class CompanyUser extends Component {
    constructor(props) {
        super(props);
        this.state = {
            options: {
                sizePerPage: 10,
                hideSizePerPage: true
            },
            companyUserId: 0,
            companyId: 0,
            createModal: false,
            editModal: false,
            deleteModal: false,
        };
        this.all = this.all.bind(this);
        this.get = this.get.bind(this);
        this.add = this.add.bind(this);
        this.edit = this.edit.bind(this);
        this.delete = this.delete.bind(this);
        this.toggleCreateModal = this.toggleCreateModal.bind(this);
        this.toggleEditModal = this.toggleEditModal.bind(this);
        this.toggleDeleteModal = this.toggleDeleteModal.bind(this);
    }

    componentDidMount() {
        if(this.props.match.params.companyId) {
            this.setState({
                companyId: this.props.match.params.companyId
            });
        }
        this.all(this.state.companyId);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.mustGetCompanyUsers) {
            companyUsers = this.props.companyUsers;
        }
        if (nextProps.companyUserCreated) {
            companyUsers = [this.props.companyUser, ...companyUsers];
            this.toggleCreateModal();
            this.props.setDefaults();
        }
        if (nextProps.companyUserEdited) {
            this.toggleEditModal();
            const index = companyUsers.findIndex(item => item.id == this.state.companyUserId);
            let companyUser = this.props.companyUser;
            companyUsers = List(companyUsers).update(index, function () {
                return companyUser;
            }).toArray();
            this.props.setDefaults();
        }
        if (nextProps.companyUserGet) {
            this.toggleEditModal();
            this.setState({
                companyUserId: this.props.companyUser.id
            });
            this.props.setDefaults();
        }
    }

    toggleCreateModal() {
        this.setState({
            createModal: !this.state.createModal
        });
    }

    toggleEditModal() {
        this.setState({
            editModal: !this.state.editModal
        });
    }

    toggleDeleteModal(id) {
        if (id) {
            this.setState({
                companyUserId: id
            });
        }
        this.setState({
            deleteModal: !this.state.deleteModal
        });
    }

    actionFormatter(cell, row) {
        return (<div><Button color={'success'} onClick={() => this.get(row.id)}>
            Edit
        </Button>{' '}
            <Button color={'danger'} onClick={() => this.toggleDeleteModal(row.id)}>
                Delete
            </Button></div>);
    }

    typeFormatter(cell, row) {
        let roleId = row.role_id;
        let type = "Driver";
        if (roleId === 3) {
            type = 'Manager';
        }
        if (roleId === 4) {
            type = 'Support';
        }
        if (roleId === 7) {
            type = 'Object Manager';
        }
        return type;
    }

    all(companyId) {
        this.props.all(companyId);
    }

    add(e) {
        e.preventDefault();
        this.props.add(this.state.companyId, this.name.value, this.type.value, this.email.value, this.username.value, this.password.value, this.confirmPassword.value);
    }

    get(id) {
        this.props.get(id);
        this.setState({companyUserId: id});
    }

    edit(e) {
        e.preventDefault();
        this.props.edit(this.state.companyUserId, this.state.companyId, this.nameEdit.value, this.typeEdit.value, this.emailEdit.value, this.usernameEdit.value, this.passwordEdit.value, this.confirmPasswordEdit.value);
    }

    delete() {
        this.props.delete(this.state.companyUserId);
        this.toggleDeleteModal();
        const index = companyUsers.findIndex(item => item.id == this.state.companyUserId);
        companyUsers = List(companyUsers).delete(index).toArray();
    }

    render() {
        return (
            <div>
                <Button color={'success'} onClick={this.toggleCreateModal}>Add Company User</Button>
                <br/>
                <br/>
                <Modal isOpen={this.state.createModal} toggle={this.toggleCreateModal} className={this.props.className}>
                    <form onSubmit={this.add}>
                        <ModalHeader toggle={this.toggleCreateModal}>Add Company User</ModalHeader>
                        <ModalBody>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.createCompanyUserErrors.name})}>
                                <input type="text" placeholder="Name"
                                       className={classNames("form-control", {'is-invalid': this.props.createCompanyUserErrors.name})}
                                       ref={(input) => this.name = input}/>
                                {this.props.createCompanyUserErrors.name ? <div className={"invalid-feedback"}>
                                    {this.props.createCompanyUserErrors.name}
                                </div> : null}
                            </div>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.createCompanyUserErrors.type})}>
                                <select
                                    className={classNames('form-control', {'is-invalid': this.props.createCompanyUserErrors.type})}
                                    ref={(input) => this.type = input}>
                                    <option value={"0"}>Select User Type</option>
                                    <option value={"3"}>Manager</option>
                                    <option value={"4"}>Support</option>
                                    <option value={"6"}>Driver</option>
                                    <option value={"7"}>Object Manager</option>
                                </select>
                                {this.props.createCompanyUserErrors.type ? <div className={"invalid-feedback"}>
                                    {this.props.createCompanyUserErrors.type}
                                </div> : null}
                            </div>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.createCompanyUserErrors.email})}>
                                <input type="text" placeholder="E-mail"
                                       className={classNames("form-control", {'is-invalid': this.props.createCompanyUserErrors.email})}
                                       ref={(input) => this.email = input}/>
                                {this.props.createCompanyUserErrors.email ? <div className={"invalid-feedback"}>
                                    {this.props.createCompanyUserErrors.email}
                                </div> : null}
                            </div>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.createCompanyUserErrors.username})}>
                                <input type="text" placeholder="Username"
                                       className={classNames("form-control", {'is-invalid': this.props.createCompanyUserErrors.username})}
                                       ref={(input) => this.username = input}/>
                                {this.props.createCompanyUserErrors.username ? <div className={"invalid-feedback"}>
                                    {this.props.createCompanyUserErrors.username}
                                </div> : null}
                            </div>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.createCompanyUserErrors.password})}>
                                <input type="password" placeholder="Password"
                                       className={classNames("form-control", {'is-invalid': this.props.createCompanyUserErrors.password})}
                                       ref={(input) => this.password = input}/>
                                {this.props.createCompanyUserErrors.password ? <div className={"invalid-feedback"}>
                                    {this.props.createCompanyUserErrors.password}
                                </div> : null}
                            </div>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.createCompanyUserErrors.confirm_password})}>
                                <input type="password" placeholder="Confirm Password"
                                       className={classNames("form-control", {'is-invalid': this.props.createCompanyUserErrors.confirm_password})}
                                       ref={(input) => this.confirmPassword = input}/>
                                {this.props.createCompanyUserErrors.confirm_password ?
                                    <div className={"invalid-feedback"}>
                                        {this.props.createCompanyUserErrors.confirm_password}
                                    </div> : null}
                            </div>
                        </ModalBody>
                        <ModalFooter>
                            <Button color="primary">Add</Button>{' '}
                            <Button color="secondary" onClick={this.toggleCreateModal}>Cancel</Button>
                        </ModalFooter>
                    </form>
                </Modal>

                <Modal isOpen={this.state.editModal} toggle={this.toggleEditModal} className={this.props.className}>
                    <form onSubmit={this.edit}>
                        <ModalHeader toggle={this.toggleEditModal}>Edit Company User</ModalHeader>
                        <ModalBody>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.editCompanyUserErrors.name})}>
                                <input type="text" placeholder="Name"
                                       className={classNames("form-control", {'is-invalid': this.props.editCompanyUserErrors.name})}
                                       ref={(input) => this.nameEdit = input}
                                       defaultValue={this.props.companyUser ? this.props.companyUser.name : ''}/>
                                {this.props.editCompanyUserErrors.name ? <div className={"invalid-feedback"}>
                                    {this.props.editCompanyUserErrors.name}
                                </div> : null}
                            </div>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.editCompanyUserErrors.type})}>
                                <select
                                    className={classNames('form-control', {'is-invalid': this.props.editCompanyUserErrors.type})}
                                    ref={(input) => this.typeEdit = input}
                                    defaultValue={this.props.companyUser ? this.props.companyUser.role_id : ''}>
                                    <option value={"0"}>Select User Type</option>
                                    <option value={"3"}>Manager</option>
                                    <option value={"4"}>Support</option>
                                    <option value={"6"}>Driver</option>
                                    <option value={"7"}>Object Manager</option>
                                </select>
                                {this.props.editCompanyUserErrors.type ? <div className={"invalid-feedback"}>
                                    {this.props.editCompanyUserErrors.type}
                                </div> : null}
                            </div>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.editCompanyUserErrors.username})}>
                                <input type="text" placeholder="Username"
                                       className={classNames("form-control", {'is-invalid': this.props.editCompanyUserErrors.username})}
                                       ref={(input) => this.domainEdit = input}
                                       defaultValue={this.props.companyUser ? this.props.companyUser.username : ''}/>
                                {this.props.editCompanyUserErrors.username ? <div className={"invalid-feedback"}>
                                    {this.props.editCompanyUserErrors.username}
                                </div> : null}
                            </div>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.editCompanyUserErrors.email})}>
                                <input type="text" placeholder="E-mail"
                                       className={classNames("form-control", {'is-invalid': this.props.editCompanyUserErrors.email})}
                                       ref={(input) => this.emailEdit = input}
                                       defaultValue={this.props.companyUser ? this.props.companyUser.email : ''}/>
                                {this.props.editCompanyUserErrors.email ? <div className={"invalid-feedback"}>
                                    {this.props.editCompanyUserErrors.email}
                                </div> : null}
                            </div>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.editCompanyUserErrors.username})}>
                                <input type="text" placeholder="Username"
                                       className={classNames("form-control", {'is-invalid': this.props.editCompanyUserErrors.username})}
                                       ref={(input) => this.usernameEdit = input}
                                       defaultValue={this.props.companyUser ? this.props.companyUser.username : ''}/>
                                {this.props.editCompanyUserErrors.username ? <div className={"invalid-feedback"}>
                                    {this.props.editCompanyUserErrors.username}
                                </div> : null}
                            </div>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.editCompanyUserErrors.password})}>
                                <input type="password" placeholder="Password"
                                       className={classNames("form-control", {'is-invalid': this.props.editCompanyUserErrors.password})}
                                       ref={(input) => this.passwordEdit = input}/>
                                {this.props.editCompanyUserErrors.password ? <div className={"invalid-feedback"}>
                                    {this.props.editCompanyUserErrors.password}
                                </div> : null}
                            </div>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.editCompanyUserErrors.confirmPassword})}>
                                <input type="password" placeholder="Confirm Password"
                                       className={classNames("form-control", {'is-invalid': this.props.editCompanyUserErrors.confirmPassword})}
                                       ref={(input) => this.confirmPasswordEdit = input}/>
                                {this.props.editCompanyUserErrors.confirmPassword ? <div className={"invalid-feedback"}>
                                    {this.props.editCompanyUserErrors.confirmPassword}
                                </div> : null}
                            </div>
                        </ModalBody>
                        <ModalFooter>
                            <Button color="primary">Save</Button>{' '}
                            <Button color="secondary" onClick={this.toggleEditModal}>Cancel</Button>
                        </ModalFooter>
                    </form>
                </Modal>


                <Modal isOpen={this.state.deleteModal} toggle={this.toggleDeleteModal}
                       className={'modal-danger ' + this.props.className}>
                    <ModalHeader toggle={this.toggleDeleteModal}>Delete Company User</ModalHeader>
                    <ModalBody>
                        Are you sure that you want delete this company user?
                    </ModalBody>
                    <ModalFooter>
                        <Button color="danger" onClick={this.delete}>Delete</Button>{' '}
                        <Button color="secondary" onClick={this.toggleDeleteModal}>Cancel</Button>
                    </ModalFooter>
                </Modal>
                <BootstrapTable data={companyUsers} version='4' options={this.state.options} pagination
                                striped={true} hover={true}>
                    <TableHeaderColumn dataField="name" isKey={true} dataSort={true}
                                       filter={{type: 'TextFilter', delay: 100}}>
                        Name</TableHeaderColumn>
                    <TableHeaderColumn dataField="type" filterFormatted={ true } dataFormat={this.typeFormatter} dataSort={true} filter={{
                        type: 'TextFilter',
                        delay: 100
                    }}>
                        Type</TableHeaderColumn>
                    <TableHeaderColumn dataField="email" dataSort={true} filter={{
                        type: 'TextFilter',
                        delay: 100
                    }}>
                        E-mail</TableHeaderColumn>
                    <TableHeaderColumn dataField='actions'
                                       dataFormat={::this.actionFormatter}>Action</TableHeaderColumn>
                </BootstrapTable>
            </div>
        )
    }
}

export default CompanyUser;
