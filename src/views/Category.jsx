import React, {Component} from 'react';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import '../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css';
import {Button, Modal, ModalHeader, ModalBody, ModalFooter} from "reactstrap";
import classNames from 'classnames';
import {List, Map} from 'immutable';
import cookie from "react-cookies";

let categories = [];
let companyLanguagesForCreate = [];
let companyLanguagesForEdit = [];
let languages = [];
let categoryNamesForCreate = {};
let categoryNamesForEdit = {};

class Category extends Component {
    constructor(props) {
        super(props);
        this.state = {
            options: {
                sizePerPage: 10,
                hideSizePerPage: true
            },
            categoryId: 0,
            category: [],
            createModal: false,
            editModal: false,
            deleteModal: false,
        };
        this.all = this.all.bind(this);
        this.getLanguages = this.getLanguages.bind(this);
        this.add = this.add.bind(this);
        this.get = this.get.bind(this);
        this.edit = this.edit.bind(this);
        this.delete = this.delete.bind(this);
        this.toggleCreateModal = this.toggleCreateModal.bind(this);
        this.toggleEditModal = this.toggleEditModal.bind(this);
        this.toggleDeleteModal = this.toggleDeleteModal.bind(this);
    }

    componentDidMount() {
        this.all();
        this.getLanguages();
    }

    componentDidUpdate() {
        if (this.props.languages) {
            languages = this.props.languages;
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.mustGetCategories) {
            categories = this.props.categories;
        }
        if (nextProps.categoryCreated) {
            categories = [this.props.category, ...categories];
            this.toggleCreateModal();
            this.props.setDefaults();
            categoryNamesForCreate = {};
        }
        if (nextProps.categoryEdited) {
            this.toggleEditModal();
            const index = categories.findIndex(item => item.id == this.state.categoryId);
            let category = this.props.category;
            categories = List(categories).update(index, function () {
                return category;
            }).toArray();
            this.props.setDefaults();
            categoryNamesForEdit = {};
        }
        if (nextProps.categoryGet) {
            this.setState({
                categoryId: this.props.category.id,
                category: nextProps.category
            });
            languages.map(function (language, key) {
                if (nextProps.languages[key].company_id == nextProps.category.id) {
                    categoryNamesForEdit[nextProps.languages[key].language_code] = nextProps.category.translations[key] ? nextProps.category.translations[key].name : '';
                }
            });
            this.toggleEditModal();
            this.props.setDefaults();
        }
    }

    toggleCreateModal() {
        this.setState({
            createModal: !this.state.createModal
        });
    }

    toggleEditModal() {
        this.setState({
            editModal: !this.state.editModal
        });
    }

    toggleDeleteModal(id) {
        if (id) {
            this.setState({
                categoryId: id
            });
        }
        this.setState({
            deleteModal: !this.state.deleteModal
        });
    }

    actionFormatter(cell, row) {
        return (<div><Button color={'success'} onClick={() => this.get(row.id)}>
            Edit
        </Button>{' '}
            <Button color={'danger'} onClick={() => this.toggleDeleteModal(row.id)}>
                Delete
            </Button></div>);
    }

    onChangeForCreate(key, e) {
        categoryNamesForCreate[languages[key].language_code] = e.target.value;
    }

    onChangeForEdit(key, e) {
        console.log(e.target.value);
        categoryNamesForEdit[languages[key].language_code] = e.target.value;
    }

    all() {
        this.props.all();
    }

    getLanguages() {
        this.props.getLanguages(0);
    }

    get(id) {
        this.props.get(id);
        this.setState({categoryId: id});
    }

    add(e) {
        e.preventDefault();
        this.props.add(categoryNamesForCreate);
    }

    edit(e) {
        e.preventDefault();
        this.props.edit(this.state.categoryId, categoryNamesForEdit);
    }

    delete() {
        this.props.delete(this.state.categoryId);
        this.toggleDeleteModal();
        const index = categories.findIndex(item => item.id == this.state.categoryId);
        categories = List(categories).delete(index).toArray();
    }

    render() {
        companyLanguagesForCreate = languages.map((language, key) =>
            <div
                className={classNames('form-group', {'has-warning': this.props.createCategoryErrors && this.props.createCategoryErrors[language.language_code]})}
                key={key}>
                <input type="text" placeholder={"Name (for:" + language.name + ")"} key={key + 1}
                       onChange={this.onChangeForCreate.bind(this, key)}
                       className={classNames("form-control", {'is-invalid': this.props.createCategoryErrors && this.props.createCategoryErrors[language.language_code]})}/>
                {this.props.createCategoryErrors && this.props.createCategoryErrors[language.language_code] ?
                    <div className={"invalid-feedback"}>
                        {this.props.createCategoryErrors ? this.props.createCategoryErrors[language.language_code] : ''}
                    </div> : null}
            </div>);

        companyLanguagesForEdit = languages.map((language, key) =>
            this.state.category.id == language.company_id ?
                <div
                    className={classNames('form-group', {'has-warning': this.props.editCategoryErrors && this.props.editCategoryErrors[language.language_code]})}
                    key={key}>
                    <input type="text" placeholder={"Name (for:" + language.name + ")"} key={key + 1}
                           onChange={this.onChangeForEdit.bind(this, key)}
                           defaultValue={this.state.category.translations && this.state.category.translations[key] ? this.state.category.translations[key].name : ''}
                           className={classNames("form-control", {'is-invalid': this.props.editCategoryErrors && this.props.editCategoryErrors[language.language_code]})}/>
                    {this.props.editCategoryErrors && this.props.editCategoryErrors[language.language_code] ?
                        <div className={"invalid-feedback"}>
                            {this.props.editCategoryErrors ? this.props.editCategoryErrors[language.language_code] : ''}
                        </div> : null}
                </div> : '');
        return (
            <div>
                {cookie.load('user').role_id == 2 || cookie.load('user').role_id == 3 ?
                    <Button color={'success'} onClick={this.toggleCreateModal} style={{'marginBottom': '20px'}}>Add
                        Category</Button>
                    : ''}
                <Modal isOpen={this.state.createModal} toggle={this.toggleCreateModal} className={this.props.className}>
                    <form onSubmit={this.add}>
                        <ModalHeader toggle={this.toggleCreateModal}>Add Category</ModalHeader>
                        <ModalBody>
                            {companyLanguagesForCreate}
                        </ModalBody>
                        <ModalFooter>
                            <Button color="primary">Add</Button>{' '}
                            <Button color="secondary" onClick={this.toggleCreateModal}>Cancel</Button>
                        </ModalFooter>
                    </form>
                </Modal>

                <Modal isOpen={this.state.editModal} toggle={this.toggleEditModal} className={this.props.className}>
                    <form onSubmit={this.edit}>
                        <ModalHeader toggle={this.toggleEditModal}>Edit Category</ModalHeader>
                        <ModalBody>
                            {companyLanguagesForEdit}
                        </ModalBody>
                        <ModalFooter>
                            <Button color="primary">Save</Button>{' '}
                            <Button color="secondary" onClick={this.toggleEditModal}>Cancel</Button>
                        </ModalFooter>
                    </form>
                </Modal>


                <Modal isOpen={this.state.deleteModal} toggle={this.toggleDeleteModal}
                       className={'modal-danger ' + this.props.className}>
                    <ModalHeader toggle={this.toggleDeleteModal}>Delete Category</ModalHeader>
                    <ModalBody>
                        Are you sure that you want delete this category?
                    </ModalBody>
                    <ModalFooter>
                        <Button color="danger" onClick={this.delete}>Delete</Button>{' '}
                        <Button color="secondary" onClick={this.toggleDeleteModal}>Cancel</Button>
                    </ModalFooter>
                </Modal>
                <BootstrapTable data={categories} version='4' options={this.state.options} pagination
                                striped={true} hover={true}>
                    <TableHeaderColumn dataField="default_name" isKey={true} dataSort={true}
                                       filter={{type: 'TextFilter', delay: 100}}>
                        Name</TableHeaderColumn>
                    <TableHeaderColumn dataField='actions'
                                       dataFormat={::this.actionFormatter}>Action</TableHeaderColumn>
                </BootstrapTable>
            </div>
        )
    }
}

export default Category;
