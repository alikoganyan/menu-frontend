import React, {Component} from 'react';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import '../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css';
import {Button, Modal, ModalHeader, ModalBody, ModalFooter} from "reactstrap";
import classNames from 'classnames';
import {List} from 'immutable';
import PlacesAutocomplete, {geocodeByAddress, getLatLng} from 'react-places-autocomplete'
import {Link} from "react-router-dom";
import cookie from "react-cookies";

let pharmacies = [];
let managers = [];

class Pharmacy extends Component {
    constructor(props) {
        super(props);
        this.state = {
            options: {
                sizePerPage: 10,
                hideSizePerPage: true
            },
            pharmacyId: 0,
            createModal: false,
            editModal: false,
            deleteModal: false,
            createPharmacyAddressName: '',
            editPharmacyAddressName: ''
        };
        this.changeCreatePharmacyAddressInput = (createPharmacyAddressName) => this.setState({createPharmacyAddressName});
        this.changeEditPharmacyAddressInput = (editPharmacyAddressName) => this.setState({editPharmacyAddressName});
        this.all = this.all.bind(this);
        this.get = this.get.bind(this);
        this.add = this.add.bind(this);
        this.edit = this.edit.bind(this);
        this.delete = this.delete.bind(this);
        this.toggleCreateModal = this.toggleCreateModal.bind(this);
        this.toggleEditModal = this.toggleEditModal.bind(this);
        this.toggleDeleteModal = this.toggleDeleteModal.bind(this);
    }

    componentDidMount() {
        this.all();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.mustGetPharmacies) {
            pharmacies = this.props.pharmacies;
        }
        if (nextProps.pharmacyCreated) {
            pharmacies = [this.props.pharmacy, ...pharmacies];
            this.toggleCreateModal();
            this.setState({
                createPharmacyAddressName: ""
            });
            this.props.setDefaults();
        }
        if (nextProps.pharmacyEdited) {
            this.toggleEditModal();
            const index = pharmacies.findIndex(item => item.id == this.state.pharmacyId);
            let pharmacy = this.props.pharmacy;
            pharmacies = List(pharmacies).update(index, function () {
                return pharmacy;
            }).toArray();
            this.props.setDefaults();
        }
        if (nextProps.pharmacyGet) {
            this.toggleEditModal();
            this.setState({
                pharmacyId: this.props.pharmacy.id,
                editPharmacyAddressName: this.props.pharmacy.address_name
            });
            this.props.setDefaults();
        }
        if (nextProps.managers) {
            managers = nextProps.managers.map((manager, key) =>
                <option key={key} value={manager.id}>{manager.name}</option>
            )
        }
    }

    toggleCreateModal() {
        this.setState({
            createModal: !this.state.createModal
        });
    }

    toggleEditModal() {
        this.setState({
            editModal: !this.state.editModal
        });
    }

    toggleDeleteModal(id) {
        if (id) {
            this.setState({
                pharmacyId: id
            });
        }
        this.setState({
            deleteModal: !this.state.deleteModal
        });
    }

    actionFormatter(cell, row) {
        return (<div><Button color={'success'} onClick={() => this.get(row.id)}>
            Edit
        </Button>{' '}
            <Button color={'danger'} onClick={() => this.toggleDeleteModal(row.id)}>
                Delete
            </Button>{' '}
            <Link to={'/product-sections/' + row.id}><Button color={'success'}>
                <i className="fa fa-dropbox"></i><br/>
            </Button></Link>
        </div>);
    }

    all() {
        this.props.all();
    }

    get(id) {
        this.props.get(id);
        this.setState({pharmacyId: id});
    }

    add(e) {
        e.preventDefault();
        let component = this;
        geocodeByAddress(this.state.createPharmacyAddressName)
            .then(results => getLatLng(results[0]))
            .then(function (latLng) {
                component.props.add(component.name.value, component.phoneNumber.value, component.state.createPharmacyAddressName, latLng.lat, latLng.lng)
            })
            .catch(function (error) {
                component.props.add(component.name.value, component.phoneNumber.value, component.state.createPharmacyAddressName, '', '');
            });
    }

    edit(e) {
        e.preventDefault();
        let component = this;
        geocodeByAddress(this.state.editPharmacyAddressName)
            .then(results => getLatLng(results[0]))
            .then(function (latLng) {
                component.props.edit(component.state.pharmacyId, component.nameEdit.value, component.phoneNumberEdit.value, component.state.editPharmacyAddressName, latLng.lat, latLng.lng);
            })
            .catch(function (error) {
                component.props.edit(component.state.pharmacyId, component.nameEdit.value, component.phoneNumberEdit.value, component.state.editPharmacyAddressName, '', '');
            });
    }

    delete() {
        this.props.delete(this.state.pharmacyId);
        this.toggleDeleteModal();
        const index = pharmacies.findIndex(item => item.id == this.state.pharmacyId);
        pharmacies = List(pharmacies).delete(index).toArray();
    }

    render() {
        const createPharmacyAddressNameInputProps = {
            value: this.state.createPharmacyAddressName,
            placeholder: 'Search Address',
            onChange: this.changeCreatePharmacyAddressInput,
        };
        const editPharmacyAddressNameInputProps = {
            value: this.state.editPharmacyAddressName,
            placeholder: 'Search Address',
            onChange: this.changeEditPharmacyAddressInput,
        };
        return (
            <div>
                {cookie.load('user').role_id == 2 || cookie.load('user').role_id == 3 ?
                    <Button color={'success'} onClick={this.toggleCreateModal} style={{'marginBottom':'20px'}}>Add Pharmacy</Button>
                    : ''}
                <br/>
                <br/>
                <Modal isOpen={this.state.createModal} toggle={this.toggleCreateModal} className={this.props.className}>
                    <form onSubmit={this.add}>
                        <ModalHeader toggle={this.toggleCreateModal}>Add Pharmacy</ModalHeader>
                        <ModalBody>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.createPharmacyErrors.name})}>
                                <input type="text" placeholder="Name"
                                       className={classNames("form-control", {'is-invalid': this.props.createPharmacyErrors.name})}
                                       ref={(input) => this.name = input}/>
                                {this.props.createPharmacyErrors.name ? <div className={"invalid-feedback"}>
                                    {this.props.createPharmacyErrors.name}
                                </div> : null}
                            </div>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.createPharmacyErrors.phone_number})}>
                                <input type="text" placeholder="Phone Number"
                                       className={classNames("form-control", {'is-invalid': this.props.createPharmacyErrors.phone_number})}
                                       ref={(input) => this.phoneNumber = input}/>
                                {this.props.createPharmacyErrors.phone_number ? <div className={"invalid-feedback"}>
                                    {this.props.createPharmacyErrors.phone_number}
                                </div> : null}
                            </div>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.createPharmacyErrors.address_name})}>
                                <PlacesAutocomplete inputProps={createPharmacyAddressNameInputProps}
                                                    classNames={{input: classNames("form-control", {'is-invalid': this.props.createPharmacyErrors.address_name || this.props.createPharmacyErrors.address_lat || this.props.createPharmacyErrors.address_lng})}}/>
                                {this.props.createPharmacyErrors.address_name || this.props.createPharmacyErrors.address_lat || this.props.createPharmacyErrors.address_lng ?
                                    <div className={"invalid-feedback"} style={{display: 'block'}}>
                                        {this.props.createPharmacyErrors.address_name}
                                    </div> : null}
                            </div>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.createPharmacyErrors.manager_id})}>
                                <select
                                    className={classNames('form-control', {'is-invalid': this.props.createPharmacyErrors.manager_id})}
                                    ref={(input) => this.manager = input}>
                                    <option value={''}>Select Manager</option>
                                    {managers}
                                </select>
                                {this.props.createPharmacyErrors.manager_id ? <div className={"invalid-feedback"}>
                                    {this.props.createPharmacyErrors.manager_id}
                                </div> : null}
                            </div>
                        </ModalBody>
                        <ModalFooter>
                            <Button color="primary">Add</Button>{' '}
                            <Button color="secondary" onClick={this.toggleCreateModal}>Cancel</Button>
                        </ModalFooter>
                    </form>
                </Modal>

                <Modal isOpen={this.state.editModal} toggle={this.toggleEditModal} className={this.props.className}>
                    <form onSubmit={this.edit}>
                        <ModalHeader toggle={this.toggleEditModal}>Edit Pharmacy</ModalHeader>
                        <ModalBody>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.editPharmacyErrors.name})}>
                                <input type="text" placeholder="Name"
                                       className={classNames("form-control", {'is-invalid': this.props.editPharmacyErrors.name})}
                                       ref={(input) => this.nameEdit = input}
                                       defaultValue={this.props.pharmacy ? this.props.pharmacy.name : ''}/>
                                {this.props.editPharmacyErrors.name ? <div className={"invalid-feedback"}>
                                    {this.props.editPharmacyErrors.name}
                                </div> : null}
                            </div>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.editPharmacyErrors.phone_number})}>
                                <input type="text" placeholder="Phone Number"
                                       className={classNames("form-control", {'is-invalid': this.props.editPharmacyErrors.phone_number})}
                                       ref={(input) => this.phoneNumberEdit = input}
                                       defaultValue={this.props.pharmacy ? this.props.pharmacy.phone_number : ''}/>
                                {this.props.editPharmacyErrors.phone_number ? <div className={"invalid-feedback"}>
                                    {this.props.editPharmacyErrors.phone_number}
                                </div> : null}
                            </div>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.editPharmacyErrors.address_name})}>
                                <PlacesAutocomplete inputProps={editPharmacyAddressNameInputProps}
                                                    classNames={{input: classNames("form-control", {'is-invalid': this.props.editPharmacyErrors.address_name || this.props.editPharmacyErrors.address_lat || this.props.editPharmacyErrors.address_lng})}}/>
                                {this.props.editPharmacyErrors.address_name || this.props.editPharmacyErrors.address_lat || this.props.editPharmacyErrors.address_lng ?
                                    <div className={"invalid-feedback"} style={{display: 'block'}}>
                                        {this.props.editPharmacyErrors.address_name}
                                    </div> : null}
                            </div>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.editPharmacyErrors.manager_id})}>
                                <select
                                    className={classNames('form-control', {'is-invalid': this.props.editPharmacyErrors.manager_id})}
                                    ref={(input) => this.manager = input}
                                    defaultValue={this.props.pharmacy ? this.props.pharmacy.manager_id : ''}>
                                    <option value={''}>Select Manager</option>
                                    {managers}
                                </select>
                                {this.props.editPharmacyErrors.manager_id ? <div className={"invalid-feedback"}>
                                    {this.props.editPharmacyErrors.manager_id}
                                </div> : null}
                            </div>
                        </ModalBody>
                        <ModalFooter>
                            <Button color="primary">Save</Button>{' '}
                            <Button color="secondary" onClick={this.toggleEditModal}>Cancel</Button>
                        </ModalFooter>
                    </form>
                </Modal>


                <Modal isOpen={this.state.deleteModal} toggle={this.toggleDeleteModal}
                       className={'modal-danger ' + this.props.className}>
                    <ModalHeader toggle={this.toggleDeleteModal}>Delete Pharmacy</ModalHeader>
                    <ModalBody>
                        Are you sure that you want delete this pharmacy?
                    </ModalBody>
                    <ModalFooter>
                        <Button color="danger" onClick={this.delete}>Delete</Button>{' '}
                        <Button color="secondary" onClick={this.toggleDeleteModal}>Cancel</Button>
                    </ModalFooter>
                </Modal>
                <BootstrapTable data={pharmacies} version='4' options={this.state.options} pagination
                                striped={true} hover={true}>
                    <TableHeaderColumn dataField="name" isKey={true} dataSort={true}
                                       filter={{type: 'TextFilter', delay: 100}}>
                        Name</TableHeaderColumn>
                    <TableHeaderColumn dataField="phone_number" dataSort={true} filter={{
                        type: 'TextFilter',
                        delay: 100
                    }}>
                        Phone Number</TableHeaderColumn>
                    <TableHeaderColumn dataField='actions'
                                       dataFormat={::this.actionFormatter}>Action</TableHeaderColumn>
                </BootstrapTable>
            </div>
        )
    }
}

export default Pharmacy;
