import React, {Component} from 'react';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import '../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css';
import {Button, Modal, ModalHeader, ModalBody, ModalFooter} from "reactstrap";
import classNames from 'classnames';
import {List} from 'immutable';
import Cropper from 'react-cropper';
import 'cropperjs/dist/cropper.css'; // see installation section above for versions of NPM older than 3.0.0
import $ from "jquery";

let products = [];
let companyLanguagesForCreate = [];
let companyLanguagesForEdit = [];
let languages = [];
let productNamesForCreate = {};
let productNamesForEdit = {};

class Product extends Component {
    constructor(props) {
        super(props);
        this.state = {
            options: {
                sizePerPage: 10,
                hideSizePerPage: true
            },
            productId: 0,
            sectionId: 0,
            product: [],
            createModal: false,
            editModal: false,
            deleteModal: false,
            file: '',
            imagePreviewUrl: '',
            imageDataUrl: ''
        };
        this.all = this.all.bind(this);
        this.getLanguages = this.getLanguages.bind(this);
        this.add = this.add.bind(this);
        this.get = this.get.bind(this);
        this.edit = this.edit.bind(this);
        this.delete = this.delete.bind(this);
        this.toggleCreateModal = this.toggleCreateModal.bind(this);
        this.toggleEditModal = this.toggleEditModal.bind(this);
        this.toggleDeleteModal = this.toggleDeleteModal.bind(this);
    }

    componentDidMount() {
        this.setState({
            sectionId: this.props.match.params.sectionId
        });
        this.all(this.props.match.params.sectionId);
        this.getLanguages();
        productNamesForCreate = {};
        productNamesForEdit = {};
    }

    componentDidUpdate() {
        if (this.props.languages) {
            languages = this.props.languages;
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.mustGetProducts) {
            products = this.props.products;
        }
        if (nextProps.productCreated) {
            products = [this.props.product, ...products];
            this.toggleCreateModal();
            this.props.setDefaults();
            productNamesForCreate = {};
            this.setState({
                file: '',
                imagePreviewUrl: '',
                imageDataUrl: ''
            });
        }
        if (nextProps.productEdited) {
            this.toggleEditModal();
            const index = products.findIndex(item => item.id == this.state.productId);
            let product = this.props.product;
            products = List(products).update(index, function () {
                return product;
            }).toArray();
            this.setState({
                imagePreviewUrl: '',
                imageDataUrl: '',
                file: '',
            });
            this.props.setDefaults();
            productNamesForEdit = {};
        }
        if (nextProps.productGet) {
            productNamesForEdit = {};
            languages.map(function (language, key) {
                if (nextProps.product.company_id == language.company_id) {
                    productNamesForEdit[nextProps.languages[key].language_code] = nextProps.product.translations[key] ? nextProps.product.translations[key].name : '';
                }
            });
            this.setState({
                productId: this.props.product.id,
                product: nextProps.product
            });
            this.props.getImageBase64(this.props.product.image);
            this.props.setDefaults();
        }
        if (nextProps.image) {
            this.setState({
                imagePreviewUrl: nextProps.image
            });
            this.toggleEditModal();
        }
    }

    toggleCreateModal() {
        this.setState({
            createModal: !this.state.createModal
        });
    }

    toggleEditModal() {
        this.setState({
            editModal: !this.state.editModal
        });
    }

    toggleDeleteModal(id) {
        if (id) {
            this.setState({
                productId: id
            });
        }
        this.setState({
            deleteModal: !this.state.deleteModal
        });
    }

    actionFormatter(cell, row) {
        return (<div><Button color={'success'} onClick={() => this.get(row.id)}>
            Edit
        </Button>{' '}
            <Button color={'danger'} onClick={() => this.toggleDeleteModal(row.id)}>
                Delete
            </Button></div>);
    }

    imageFormatter(cell, row) {
        return (<img src={row.image} style={{'max-height': '200px'}}/>)
    }

    clickProductUploadImageButton() {
        $('.uploadFile').click();
    }

    _crop() {
        this.setState({
            imageDataUrl: this.refs.cropper.getCroppedCanvas().toDataURL()
        });
    }

    _handleCreateProductImageChange(e) {
        e.preventDefault();
        let reader = new FileReader();
        let file = e.target.files[0];
        if (file.type == 'image/png' || file.type == 'image/jpg' || file.type == 'image/jpeg') {
            reader.onloadend = () => {
                this.setState({
                    file: file,
                    imagePreviewUrl: reader.result
                });
            };
            reader.readAsDataURL(file);
        } else {
            alert('Please upload PNG or JPG');
        }
    }

    onChangeForCreate(key, e) {
        productNamesForCreate[languages[key].language_code] = e.target.value;
    }

    onChangeForEdit(key, e) {
        productNamesForEdit[languages[key].language_code] = e.target.value;
    }

    all(sectionId) {
        this.props.all(sectionId);
    }

    getLanguages() {
        this.props.getLanguages(0);
    }

    get(id) {
        this.props.get(id);
        this.setState({productId: id});
    }

    add(e) {
        e.preventDefault();
        this.props.add(this.state.sectionId, productNamesForCreate, this.state.imageDataUrl, this.price.value, this.discount.value);
    }

    edit(e) {
        e.preventDefault();
        this.props.edit(this.state.productId, this.state.sectionId, productNamesForEdit, this.state.imageDataUrl, this.priceEdit.value, this.discountEdit.value);
    }

    delete() {
        this.props.delete(this.state.productId);
        this.toggleDeleteModal();
        const index = products.findIndex(item => item.id == this.state.productId);
        products = List(products).delete(index).toArray();
    }

    render() {
        companyLanguagesForCreate = languages.map((language, key) =>
            <div
                className={classNames('form-group', {'has-warning': this.props.createProductErrors && this.props.createProductErrors['names.' + language.language_code]})}
                key={key}>
                <input type="text" placeholder={"Name (for:" + language.name + ")"} key={key + 1}
                       onChange={this.onChangeForCreate.bind(this, key)}
                       className={classNames("form-control", {'is-invalid': this.props.createProductErrors && this.props.createProductErrors['names.' + language.language_code]})}/>
                {this.props.createProductErrors && this.props.createProductErrors['names.' + language.language_code] ?
                    <div className={"invalid-feedback"}>
                        {this.props.createProductErrors ? this.props.createProductErrors['names.' + language.language_code] : ''}
                    </div> : null}
            </div>);

        companyLanguagesForEdit = languages.map((language, key) =>
            this.state.product.company_id == language.company_id ?
                <div
                    className={classNames('form-group', {'has-warning': this.props.editProductErrors && this.props.editProductErrors['names.' + language.language_code]})}
                    key={key}>
                    <input type="text" placeholder={"Name (for:" + language.name + ")"} key={key + 1}
                           onChange={this.onChangeForEdit.bind(this, key)}
                           defaultValue={this.state.product.translations && this.state.product.translations[key] ? this.state.product.translations[key].name : ''}
                           className={classNames("form-control", {'is-invalid': this.props.editProductErrors && this.props.editProductErrors['names.' + language.language_code]})}/>
                    {this.props.editProductErrors && this.props.editProductErrors['names.' + language.language_code] ?
                        <div className={"invalid-feedback"}>
                            {this.props.editProductErrors ? this.props.editProductErrors['names.' + language.language_code] : ''}
                        </div> : null}
                </div> : '');
        return (
            <div>
                <Button color={'success'} onClick={this.toggleCreateModal}>Add Product</Button>
                <br/>
                <br/>
                <Modal isOpen={this.state.createModal} toggle={this.toggleCreateModal} className={this.props.className}>
                    <form onSubmit={this.add}>
                        <ModalHeader toggle={this.toggleCreateModal}>Add Product</ModalHeader>
                        <ModalBody>
                            {companyLanguagesForCreate}
                            <input type={'file'} accept={'image/jpeg,image/png'} className={'uploadFile'}
                                   style={{'display': 'none'}}
                                   onChange={(e) => this._handleCreateProductImageChange(e)}/>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.createProductErrors && this.props.createProductErrors.image})}>
                                <Button color={'success'}
                                        onClick={this.clickProductUploadImageButton}>{'Upload File'}</Button>
                                {this.state.file ?
                                    <div>
                                        <br/>
                                        <Cropper
                                            ref='cropper'
                                            src={this.state.imagePreviewUrl}
                                            style={{height: 400, width: '100%'}}
                                            crop={this._crop.bind(this)}
                                            aspectRatio={16 / 9}
                                            guides={false}/>
                                    </div>
                                    : null}
                                <input type="hidden"
                                       className={classNames("form-control", {'is-invalid': this.props.createProductErrors && this.props.createProductErrors.image})}/>
                                {this.props.createProductErrors && this.props.createProductErrors.image ?
                                    <div className={"invalid-feedback"}>
                                        {this.props.createProductErrors ? this.props.createProductErrors.image : ''}
                                    </div> : null}
                            </div>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.createProductErrors && this.props.createProductErrors.price})}>
                                <input type="text" placeholder={"Price"}
                                       ref={(input) => this.price = input}
                                       className={classNames("form-control", {'is-invalid': this.props.createProductErrors && this.props.createProductErrors.price})}/>
                                {this.props.createProductErrors && this.props.createProductErrors.price ?
                                    <div className={"invalid-feedback"}>
                                        {this.props.createProductErrors ? this.props.createProductErrors.price : ''}
                                    </div> : null}
                            </div>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.createProductErrors && this.props.createProductErrors.discount})}>
                                <input type="text" placeholder={"Discount"}
                                       ref={(input) => this.discount = input}
                                       defaultValue={'0'}
                                       className={classNames("form-control", {'is-invalid': this.props.createProductErrors && this.props.createProductErrors.discount})}/>
                                {this.props.createProductErrors && this.props.createProductErrors.discount ?
                                    <div className={"invalid-feedback"}>
                                        {this.props.createProductErrors ? this.props.createProductErrors.discount : ''}
                                    </div> : null}
                            </div>
                        </ModalBody>
                        <ModalFooter>
                            <Button color="primary">Add</Button>{' '}
                            <Button color="secondary" onClick={this.toggleCreateModal}>Cancel</Button>
                        </ModalFooter>
                    </form>
                </Modal>

                <Modal isOpen={this.state.editModal} toggle={this.toggleEditModal} className={this.props.className}>
                    <form onSubmit={this.edit}>
                        <ModalHeader toggle={this.toggleEditModal}>Edit Product Section</ModalHeader>
                        <ModalBody>
                            {companyLanguagesForEdit}
                            <input type={'file'} accept={'image/jpeg,image/png'} className={'uploadFile'}
                                   style={{'display': 'none'}}
                                   onChange={(e) => this._handleCreateProductImageChange(e)}/>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.editProductErrors && this.props.editProductErrors.image})}>
                                <Button color={'success'}
                                        onClick={this.clickProductUploadImageButton}>{'Upload File'}</Button>
                                <div>
                                    <br/>
                                    <Cropper
                                        ref='cropper'
                                        src={this.state.imagePreviewUrl}
                                        style={{height: 400, width: '100%'}}
                                        crop={this._crop.bind(this)}
                                        aspectRatio={16 / 9}
                                        guides={false}/>
                                </div>
                                <input type="hidden"
                                       className={classNames("form-control", {'is-invalid': this.props.editProductErrors && this.props.editProductErrors.image})}/>
                                {this.props.editProductErrors && this.props.editProductErrors.image ?
                                    <div className={"invalid-feedback"}>
                                        {this.props.editProductErrors ? this.props.editProductErrors.image : ''}
                                    </div> : null}
                            </div>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.editProductErrors && this.props.editProductErrors.price})}>
                                <input type="text" placeholder={"Price"}
                                       ref={(input) => this.priceEdit = input}
                                       defaultValue={this.props.product ? this.props.product.price : ''}
                                       className={classNames("form-control", {'is-invalid': this.props.editProductErrors && this.props.editProductErrors.price})}/>
                                {this.props.editProductErrors && this.props.editProductErrors.price ?
                                    <div className={"invalid-feedback"}>
                                        {this.props.editProductErrors ? this.props.editProductErrors.price : ''}
                                    </div> : null}
                            </div>
                            <div
                                className={classNames('form-group', {'has-warning': this.props.editProductErrors && this.props.editProductErrors.discount})}>
                                <input type="text" placeholder={"Discount"}
                                       ref={(input) => this.discountEdit = input}
                                       defaultValue={this.props.product ? this.props.product.discount : '0'}
                                       className={classNames("form-control", {'is-invalid': this.props.createProductErrors && this.props.editProductErrors.discount})}/>
                                {this.props.editProductErrors && this.props.editProductErrors.discount ?
                                    <div className={"invalid-feedback"}>
                                        {this.props.editProductErrors ? this.props.editProductErrors.discount : ''}
                                    </div> : null}
                            </div>
                        </ModalBody>
                        <ModalFooter>
                            <Button color="primary">Save</Button>{' '}
                            <Button color="secondary" onClick={this.toggleEditModal}>Cancel</Button>
                        </ModalFooter>
                    </form>
                </Modal>


                <Modal isOpen={this.state.deleteModal} toggle={this.toggleDeleteModal}
                       className={'modal-danger ' + this.props.className}>
                    <ModalHeader toggle={this.toggleDeleteModal}>Delete Product Section</ModalHeader>
                    <ModalBody>
                        Are you sure that you want delete this product section?
                    </ModalBody>
                    <ModalFooter>
                        <Button color="danger" onClick={this.delete}>Delete</Button>{' '}
                        <Button color="secondary" onClick={this.toggleDeleteModal}>Cancel</Button>
                    </ModalFooter>
                </Modal>
                <BootstrapTable data={products} version='4' options={this.state.options} pagination
                                striped={true} hover={true}>
                    <TableHeaderColumn dataField="default_name" isKey={true} dataSort={true}
                                       filter={{type: 'TextFilter', delay: 100}}>
                        Name</TableHeaderColumn>
                    <TableHeaderColumn dataField="image" dataFormat={this.imageFormatter}>
                        Image</TableHeaderColumn>
                    <TableHeaderColumn dataField='actions'
                                       dataFormat={::this.actionFormatter}>Action</TableHeaderColumn>
                </BootstrapTable>
            </div>
        )
    }
}

export default Product;
