import React, {Component} from 'react';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import '../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css';
import {Button, Modal, ModalHeader, ModalBody, ModalFooter} from "reactstrap";
import classNames from 'classnames';
import {List} from 'immutable';
import {Link} from "react-router-dom";
import cookie from "react-cookies";

let productSections = [];
let companyLanguagesForCreate = [];
let companyLanguagesForEdit = [];
let languages = [];
let productSectionNamesForCreate = {};
let productSectionNamesForEdit = {};

class ProductSection extends Component {
    constructor(props) {
        super(props);
        this.state = {
            options: {
                sizePerPage: 10,
                hideSizePerPage: true
            },
            productSectionId: 0,
            objectId: 0,
            productSection: [],
            createModal: false,
            editModal: false,
            deleteModal: false,
        };
        this.all = this.all.bind(this);
        this.getLanguages = this.getLanguages.bind(this);
        this.add = this.add.bind(this);
        this.get = this.get.bind(this);
        this.edit = this.edit.bind(this);
        this.delete = this.delete.bind(this);
        this.toggleCreateModal = this.toggleCreateModal.bind(this);
        this.toggleEditModal = this.toggleEditModal.bind(this);
        this.toggleDeleteModal = this.toggleDeleteModal.bind(this);
    }

    componentDidMount() {
        this.setState({
            objectId: this.props.match.params.objectId
        });
        this.all(this.props.match.params.objectId);
        this.getLanguages();
        productSectionNamesForCreate['names'] = {};
        productSectionNamesForCreate['object_id'] = this.props.match.params.objectId;
        productSectionNamesForEdit['names'] = {};
        productSectionNamesForEdit['object_id'] = this.props.match.params.objectId;
    }

    componentDidUpdate() {
        if (this.props.languages) {
            languages = this.props.languages;
        }
    }

    componentWillReceiveProps(nextProps) {
        console.log(nextProps);
        if (nextProps.mustGetProductSections) {
            productSections = this.props.productSections;
        }
        if (nextProps.productSectionCreated) {
            productSections = [this.props.productSection, ...productSections];
            this.toggleCreateModal();
            this.props.setDefaults();
            productSectionNamesForCreate['names'] = {};
        }
        if (nextProps.productSectionEdited) {
            this.toggleEditModal();
            const index = productSections.findIndex(item => item.id == this.state.productSectionId);
            let productSection = this.props.productSection;
            productSections = List(productSections).update(index, function () {
                return productSection;
            }).toArray();
            this.props.setDefaults();
            productSectionNamesForEdit['names'] = {};
        }
        if (nextProps.productSectionGet) {
            productSectionNamesForEdit['names'] = {};
            languages.map(function (language, key) {
                if (nextProps.productSection.company_id == language.company_id) {
                    productSectionNamesForEdit['names'][nextProps.languages[key].language_code] = nextProps.productSection.translations[key] ? nextProps.productSection.translations[key].name : ''
                }
            });
            this.toggleEditModal();
            this.setState({
                productSectionId: this.props.productSection.id,
                productSection: nextProps.productSection
            });
            this.props.setDefaults();
        }
    }

    toggleCreateModal() {
        this.setState({
            createModal: !this.state.createModal
        });
    }

    toggleEditModal() {
        this.setState({
            editModal: !this.state.editModal
        });
    }

    toggleDeleteModal(id) {
        if (id) {
            this.setState({
                productSectionId: id
            });
        }
        this.setState({
            deleteModal: !this.state.deleteModal
        });
    }

    actionFormatter(cell, row) {
        return (<div><Button color={'success'} onClick={() => this.get(row.id)}>
            Edit
        </Button>{' '}
            <Button color={'danger'} onClick={() => this.toggleDeleteModal(row.id)}>
                Delete
            </Button></div>);
    }

    nameFormatter(cell, row) {
        return (<Link to={"/products/" + row.id}>{row.default_name}</Link>)
    }

    onChangeForCreate(key, e) {
        productSectionNamesForCreate['names'][languages[key].language_code] = e.target.value;
    }

    onChangeForEdit(key, e) {
        productSectionNamesForEdit['names'][languages[key].language_code] = e.target.value;
    }

    all(objectId) {
        this.props.all(objectId);
    }

    getLanguages() {
        this.props.getLanguages(0);
    }

    get(id) {
        this.props.get(id);
        this.setState({productSectionId: id});
    }

    add(e) {
        e.preventDefault();
        this.props.add(productSectionNamesForCreate);
    }

    edit(e) {
        e.preventDefault();
        this.props.edit(this.state.productSectionId, productSectionNamesForEdit);
    }

    delete() {
        this.props.delete(this.state.productSectionId);
        this.toggleDeleteModal();
        const index = productSections.findIndex(item => item.id == this.state.productSectionId);
        productSections = List(productSections).delete(index).toArray();
    }

    render() {
        companyLanguagesForCreate = languages.map((language, key) =>
            <div
                className={classNames('form-group', {'has-warning': this.props.createProductSectionErrors && this.props.createProductSectionErrors['names.' + language.language_code]})}
                key={key}>
                <input type="text" placeholder={"Name (for:" + language.name + ")"} key={key + 1}
                       onChange={this.onChangeForCreate.bind(this, key)}
                       className={classNames("form-control", {'is-invalid': this.props.createProductSectionErrors && this.props.createProductSectionErrors['names.' + language.language_code]})}/>
                {this.props.createProductSectionErrors && this.props.createProductSectionErrors['names.' + language.language_code] ?
                    <div className={"invalid-feedback"}>
                        {this.props.createProductSectionErrors ? this.props.createProductSectionErrors['names.' + language.language_code] : ''}
                    </div> : null}
            </div>);

        companyLanguagesForEdit = languages.map((language, key) =>
            this.state.productSection.company_id == language.company_id ?
                <div
                    className={classNames('form-group', {'has-warning': this.props.editProductSectionErrors && this.props.editProductSectionErrors['names.' + language.language_code]})}
                    key={key}>
                    <input type="text" placeholder={"Name (for:" + language.name + ")"} key={key + 1}
                           onChange={this.onChangeForEdit.bind(this, key)}
                           defaultValue={this.state.productSection.translations && this.state.productSection.translations[key] ? this.state.productSection.translations[key].name : ''}
                           className={classNames("form-control", {'is-invalid': this.props.editProductSectionErrors && this.props.editProductSectionErrors['names.' + language.language_code]})}/>
                    {this.props.editProductSectionErrors && this.props.editProductSectionErrors['names.' + language.language_code] ?
                        <div className={"invalid-feedback"}>
                            {this.props.editProductSectionErrors ? this.props.editProductSectionErrors['names.' + language.language_code] : ''}
                        </div> : null}
                </div> : '');
        return (
            <div>
                {cookie.load('user').role_id == 2 || cookie.load('user').role_id == 3 ?
                    <Button color={'success'} onClick={this.toggleCreateModal} style={{'marginBottom': '20px'}}>Add
                        Product Section</Button>
                    : ''}
                <Modal isOpen={this.state.createModal} toggle={this.toggleCreateModal} className={this.props.className}>
                    <form onSubmit={this.add}>
                        <ModalHeader toggle={this.toggleCreateModal}>Add Product Section</ModalHeader>
                        <ModalBody>
                            {companyLanguagesForCreate}
                        </ModalBody>
                        <ModalFooter>
                            <Button color="primary">Add</Button>{' '}
                            <Button color="secondary" onClick={this.toggleCreateModal}>Cancel</Button>
                        </ModalFooter>
                    </form>
                </Modal>

                <Modal isOpen={this.state.editModal} toggle={this.toggleEditModal} className={this.props.className}>
                    <form onSubmit={this.edit}>
                        <ModalHeader toggle={this.toggleEditModal}>Edit Product Section</ModalHeader>
                        <ModalBody>
                            {companyLanguagesForEdit}
                        </ModalBody>
                        <ModalFooter>
                            <Button color="primary">Save</Button>{' '}
                            <Button color="secondary" onClick={this.toggleEditModal}>Cancel</Button>
                        </ModalFooter>
                    </form>
                </Modal>


                <Modal isOpen={this.state.deleteModal} toggle={this.toggleDeleteModal}
                       className={'modal-danger ' + this.props.className}>
                    <ModalHeader toggle={this.toggleDeleteModal}>Delete Product Section</ModalHeader>
                    <ModalBody>
                        Are you sure that you want delete this product section?
                    </ModalBody>
                    <ModalFooter>
                        <Button color="danger" onClick={this.delete}>Delete</Button>{' '}
                        <Button color="secondary" onClick={this.toggleDeleteModal}>Cancel</Button>
                    </ModalFooter>
                </Modal>
                <BootstrapTable data={productSections} version='4' options={this.state.options} pagination
                                striped={true} hover={true}>
                    <TableHeaderColumn dataFormat={this.nameFormatter} dataField="default_name" isKey={true}
                                       dataSort={true}
                                       filter={{type: 'TextFilter', delay: 100}}>
                        Name</TableHeaderColumn>
                    <TableHeaderColumn dataField='actions'
                                       dataFormat={::this.actionFormatter}>Action</TableHeaderColumn>
                </BootstrapTable>
            </div>
        )
    }
}

export default ProductSection;
